#ifndef _EEPROM_H_
#define _EEPROM_H_

void eeprom_read_block (void *__dst, const void *__src, size_t __n);
void eeprom_update_block (const void *__src, void *__dst, size_t __n);
void eeprom_register_block(void *src, size_t size);

#endif /*_EEPROM_H_*/
