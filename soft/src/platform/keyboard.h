#ifndef _AVR_KEYBOARD_H_
#define _AVR_KEYBOARD_H_

void avr_keyboard_init();
void avr_set_key_handler(void (*key_handler)(char flags));

#endif /*_AVR_KEYBOARD_H_*/
