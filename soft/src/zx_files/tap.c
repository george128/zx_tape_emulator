#include <string.h>
#include <platform/utils.h>

#include "tap.h"

//#define TAP_DEBUG 2

#if TAP_DEBUG > 1
#define DEBUG_PRINT_2(h, str,...) if(h!=NULL)h(str, __VA_ARGS__)
#else
#define DEBUG_PRINT_2(handler, str,...)
#endif

char tap_process(const struct tap_handler *handler, struct tap_state *state)
{
    short rb;

    if(state->block_len==0) {
        memset(state->buf,0, TAP_BUF_SIZE);
        rb=sizeof(unsigned short); /*read len*/
        state->is_first=1;
        state->block_pos=0;
        rb=handler->get_next_data((char *)&state->block_len, rb);
        if(rb==0) {
            DEBUG_PRINT_2(handler->log, "No next data\n",NULL);
            return TAP_STATE_DONE;
        }
        if(rb<0)
            return TAP_STATE_ERR;
        state->file_pos+=rb;
    } else {
        state->is_first=0;
    }
    DEBUG_PRINT_2(handler->log, "is_first=%d block_len=%d block_pos=%d\n", 
                     state->is_first, 
                     state->block_len, 
                     state->block_pos);
    if(state->block_len<TAP_BUF_SIZE) {
        rb=state->block_len;
        DEBUG_PRINT_2(handler->log, "rb1=%d\n", rb);
    } else if(state->block_len-state->block_pos<TAP_BUF_SIZE) {
        rb=state->block_len-state->block_pos;
        DEBUG_PRINT_2(handler->log, "rb2=%d\n", rb);
    } else {
        rb=TAP_BUF_SIZE;
        DEBUG_PRINT_2(handler->log, "rb3=%d\n", rb);
    }

    state->buf_len=handler->get_next_data(state->buf, rb);

    if(state->buf_len<=0)
        return TAP_STATE_ERR;

    state->block_pos+=state->buf_len;
    if(state->is_first && *state->buf==0 && state->block_len==0x13) {
        struct tap_header *header = (struct tap_header *)(state->buf);
        handler->found_header(header, state->file_pos-sizeof(unsigned short));
    } else if(state->is_first) {
        handler->found_block(state->block_len, state->file_pos-sizeof(unsigned short));
    }
    state->file_pos+=state->buf_len;
    return TAP_STATE_PROCESS;
}
