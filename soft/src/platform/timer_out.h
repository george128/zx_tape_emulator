#ifndef _TIMER_OUT_H_
#define _TIMER_OUT_H_

#include <platform/load_save_params.h>

#define PILOT_TYPE_NONE 0
#define PILOT_TYPE_HEAD 1
#define PILOT_TYPE_DATA 2

#define TIMER_OUT_RES_OK 0
#define TIMER_OUT_RES_BUSY 1
#define TIMER_OUT_RES_ERR -1

#define TAPE_OUT_BUF_SIZE 256

struct outbuf {
    char buf[TAPE_OUT_BUF_SIZE];
    unsigned short pos, len;
    char flags;
};

struct timer_out_state {
    struct outbuf outbuf[2];
    volatile char state, bit_pos,buf_no;
    volatile unsigned char pin_state;
    volatile unsigned short pilot_pos;
    struct params_load params_load;
    char sound_on;
};


void timer_out_init(struct timer_out_state *state);
void timer_out_done();
char timer_out_process_block(char pilot_type,char *buf, unsigned short buf_len);

#endif /*_TIMER_OUT_H_*/
