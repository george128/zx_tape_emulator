#ifndef _UART_H_
#define _UART_H_

#define MAX_UARTS 5

struct uart_dev {
    void (*init)();
    void (*putc)(char c);
    char (*input_check)(char timeout_ms);
    char (*readc)();
    const char *name;
};

char register_uart(const struct uart_dev *dev);

void uart_init();

void uart_putc(char uart_no, char ch);
void uart_puts(char uart_no, const char *str);
void uart_put_mem(char uart_no, const char *buf, char len);
void uart_puts_P(char uart_no, const char *str);
void uart_putd(char uart_no, char byte);
void uart_putd10(char uart_no, char byte);
void uart_put_short(char uart_no, unsigned short d);
void uart_put_long(char uart_no, unsigned long d);

char uart_read(char uart_no, char timeout_ms);
char uart_read_line(char uart_no, char *buf, char buf_len);

#define uart_next_line(uart_no) uart_putc(uart_no, '\n')
#define uart_put_msg(uart_no, msg, val) \
    uart_puts_P(uart_no, msg);\
    uart_putd(uart_no, val);\
    uart_next_line(uart_no)
#define uart_put_msg_short(uart_no, msg, val) \
    uart_puts_P(uart_no, msg);\
    uart_put_short(uart_no, val);\
    uart_next_line(uart_no)


#endif /*_UART_H_*/
