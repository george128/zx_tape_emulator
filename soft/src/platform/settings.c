#include <platform/utils.h>
#include "settings.h"
#include <console/console.h>

const static PROGMEM char SETTINGS_LOAD_FALLBACK[] = "Loading fallback settings: ";
const static PROGMEM char SETTINGS_SAVE[] = "Saving settings: ";

const static PROGMEM struct settings settings_fallback = {
    .sound_on=1,
    .display_rotate=0,
    .lcd_contrast=0x42,
    .lcd_backlight=0,
};

static EEMEM struct settings settings_eeprom;

void get_settings(struct settings *s)
{
    eeprom_read_block(s, &settings_eeprom, sizeof(settings_eeprom));
    if(((unsigned char)s->sound_on)==0xff) {
        memcpy_P(s, &settings_fallback, sizeof(settings_fallback));
        eeprom_write_block(s, &settings_eeprom, sizeof(settings_eeprom));
        console_puts_P(SETTINGS_LOAD_FALLBACK);
        console_putmem(s, sizeof(*s));
        console_next_line();
    }
}

void save_settings(struct settings *s)
{
    eeprom_write_block(s, &settings_eeprom, sizeof(settings_eeprom));
    console_puts_P(SETTINGS_SAVE);
    console_putmem(s, sizeof(*s));
    console_next_line();
}
