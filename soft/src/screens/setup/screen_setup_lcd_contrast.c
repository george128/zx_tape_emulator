#include "screen_setup_lcd_contrast.h"

#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <screens/setup/module_setup.h>
#include <graphics/graphics.h>
#include <keyboard/keyboard.h>
#include <widgets/list.h>
#include <platform/utils.h>
#include <platform/settings.h>
#include <platform/adc.h>
#include <platform/nokia5110.h>
#include <utils/strconv.h>
#include <timer/timer.h>

PROGMEM static const char STR_SCREEN_SETUP_LCD_CONTRAST[]  ="LCD contrast";

#define NOISE_MASK (~3)

struct data {
    struct settings settings;
    unsigned char last_value;
    char is_draw;
    char timer_id;
    struct timer t;
};

#define shm ((struct data *) (get_shared_mem()))

static void screen_setup_lcd_contrast_draw_text()
{
    char buf[6];

    clear_line(2);
    byte2str(shm->settings.lcd_contrast,buf);
    draw_text(size_x(5),line_y(2),buf,0,0);
    flush_screen();
}

static void screen_setup_lcd_contrast_timer_callback(unsigned char timer_id, const void *data) 
{
    unsigned char value = adc_get_value() & NOISE_MASK;
    if(value != shm->last_value) {
        shm->last_value = value;
        shm->settings.lcd_contrast=value;
        screen_setup_lcd_contrast_draw_text();
        nokia_lcd_set_contrast(value);
    }
}

static void screen_setup_lcd_contrast_display() 
{
    adc_init();
    get_settings(&shm->settings);
    clear_screen();
    draw_text(size_x(0),line_y(1),STR_SCREEN_SETUP_LCD_CONTRAST,0,1);
    screen_setup_lcd_contrast_draw_text();
    shm->t.period=200;
    shm->t.callback=screen_setup_lcd_contrast_timer_callback;
    shm->timer_id=timer_setup(&shm->t);
}

static void screen_setup_lcd_contrast_destroy() 
{
    if(shm->timer_id != 0)
        timer_free(shm->timer_id);
    adc_stop();
}

static void screen_setup_lcd_contrast_save_settings()
{
    struct settings s;
    unsigned char value = adc_get_value() & NOISE_MASK;

    get_settings(&s);
    s.lcd_contrast=value;
    save_settings(&s);
}

static void screen_setup_lcd_contrast_key_press(char keycode)
{
    switch(keycode) {
        case KEY_LEFT:
            module_process_event(PRIVATE_EVENT_SCREEN_SETUP_MAIN);
            break;
        case KEY_ENTER:
            screen_setup_lcd_contrast_save_settings();
            break;
    }

}

PROGMEM const struct screen screen_setup_lcd_contrast =
{
    .display=screen_setup_lcd_contrast_display,
    .destroy=screen_setup_lcd_contrast_destroy,
    .key_press=screen_setup_lcd_contrast_key_press,
    .shm_size=sizeof(struct data)
};
