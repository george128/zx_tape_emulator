#include <string.h>
#include <console/console.h>
#include <graphics/graphics.h>
#include <keyboard/keyboard.h>
#include <utils/strconv.h>
#include <screens/scr_module.h>

#include "screen_setup_params_base.h"
#include "module_setup.h"

#undef PARAMS_BASE_DEBUG

PROGMEM const char STR_POSTFIX_US[]= "us";
PROGMEM const char STR_POSTFIX_MS[]= "ms";
PROGMEM const char STR_POSTFIX_S[] = "s";

void params_char_to_str(void *val, char *buf,void (*conv_to)(void *val))
{
    char ch=*(char *)val;
#ifdef PARAMS_BASE_DEBUG
    console_putd10(ch);
#endif
    if(conv_to != NULL)
        conv_to(&ch);
    byte2dec(ch, buf);
#ifdef PARAMS_BASE_DEBUG
    console_putc(' ');
    console_puts(buf);
#endif
}

void params_char_from_str(char *buf, void *val, void (*conv_from)(void *val)) 
{
    char ch=str10_2byte(buf);
    if(conv_from != NULL)
        conv_from(&ch);
    *(char *)val=(char)(ch&0xff);
}

void params_short_to_str(void *val, char *buf, void (*conv_to)(void *val))
{
    unsigned short us = *(unsigned short *)val;
#ifdef PARAMS_BASE_DEBUG
    console_put_short(us);
#endif
    if(conv_to != NULL)
        conv_to(&us);
    short2dec(us, buf);
#ifdef PARAMS_BASE_DEBUG
    console_putc(' ');
    console_puts(buf);
#endif
}

void params_short_from_str(char *buf, void *val, void (*conv_from)(void *val)) 
{
    short sh=dec2short(buf);
    console_puts(buf);
    console_putc(' ');
    console_put_short(sh);
    console_putc(' ');
    if(conv_from != NULL)
        conv_from(&sh);
    console_put_short(sh);
    *(unsigned short *)val=sh;
}

void params_conv_to_us(void *val)
{
    unsigned short us=*(unsigned short *)val;
    *(unsigned short *)val=(us+8)/16;
}

void params_conv_from_us(void *val)
{
    unsigned short us=*(unsigned short *)val;
    *(unsigned short *)val=us*16;
}


static void screen_setup_params_show_list(struct params_data *data)
{
    char i, *ptr;
    struct param_rec rec;
    char buf[10];

    clear_screen();
#ifdef PARAMS_BASE_DEBUG
    console_puts("offset=");
    console_putd10(data->offset);
    console_next_line();
#endif
    for(i=0;i<SCREEN_MAX_LINES;i++) {
        memcpy_P(&rec, &data->param_recs[i+data->offset], sizeof(rec));
        draw_text(0,line_y(i),rec.text,0,1);
        ptr=(char *)data->params;
        ptr+=rec.offset;
#ifdef PARAMS_BASE_DEBUG
        console_puts_P(rec.text);
        console_puts(" i=");
        console_putd10(i);
        console_putc(' ');
#endif        
        rec.to_str(ptr, buf, rec.conv_to);
        draw_text(size_x(8),line_y(i),buf, 0, 0);
        if(rec.postfix != NULL)
            draw_text(size_x(8+strlen(buf)),line_y(i),rec.postfix,0,1);
#ifdef PARAMS_BASE_DEBUG
        console_next_line();
#endif
    }
    if(data->list.num_elements!=0)
        g_list_redraw(&data->list);
}

void screen_setup_params_init(struct params_data *data, const struct param_rec *recs, char num_recs) 
{
    data->list.num_elements=0;
    data->offset=0;
    data->is_edit=0;
    data->param_recs=recs;
    data->num_recs=num_recs;
    data->get_params(data->params);
    screen_setup_params_show_list(data);
    data->list.num_elements=SCREEN_MAX_LINES;
    data->list.is_vert=1;
    data->list.width=SCREEN_WIDTH-1;
    data->list.x_start=0;
    data->list.y_start=0;
    g_list_init(&data->list, 0);
}

static void screen_setup_params_process_entry(struct params_data *data) {
    char pos = data->offset+g_list_get_current(&data->list);
    struct param_rec rec;
    char *ptr;
    
    memcpy_P(&rec, &data->param_recs[pos], sizeof(rec));
    ptr=(char *)data->params;
    ptr+=rec.offset;
    rec.to_str(ptr, data->keyb_buf, rec.conv_to);
    g_keyb_init(&data->keyb, data->keyb_buf, 9, G_KEYB_TYPE_DGT);
    data->is_edit=1;
}

static void screen_setup_params_save(struct params_data *data)
{
    char pos = data->offset+g_list_get_current(&data->list);
    struct param_rec rec;
    char *ptr;

    memcpy_P(&rec, &data->param_recs[pos], sizeof(rec));
    ptr=(char *)data->params;
    ptr+=rec.offset;
    rec.from_str(data->keyb_buf, ptr, rec.conv_from);
    data->save_params(data->params);
}

static char screen_setup_params_chk_list_update(struct params_data *data, char keycode) 
{
    switch(keycode) {
	case KEY_UP:
	    return data->offset>0 
		&& g_list_get_current(&data->list) == 0;
	case KEY_DOWN:
	    return data->offset < data->num_recs-data->list.num_elements
		&& g_list_get_current(&data->list)+1 == data->list.num_elements;
    }
    return 0;
}

void screen_setup_params_key_press(struct params_data *data, char keycode)
{
    if(data->is_edit) {
        switch(g_keyb_process_key(&data->keyb, keycode)) {
            case G_KEYB_EDIT:
                break;
            case G_KEYB_DONE:
                data->is_edit=0;
                screen_setup_params_save(data);
                screen_setup_params_show_list(data);
                break;
            case G_KEYB_CANCEL:
                data->is_edit=0;
                screen_setup_params_show_list(data);
                break;
        }
        return;
    }
    switch(keycode) {
        case KEY_ENTER:
            screen_setup_params_process_entry(data);
            break;
        case KEY_UP:
            if(screen_setup_params_chk_list_update(data, keycode)) {
                data->offset--;
                screen_setup_params_show_list(data);
            } else 
                g_list_up(&data->list);
            break;
        case KEY_DOWN:
            if(screen_setup_params_chk_list_update(data, keycode)) {
                data->offset++;
                screen_setup_params_show_list(data);
            } else 
                g_list_down(&data->list);
            break;
        case KEY_RIGHT:
            break;
        case KEY_LEFT:
            module_process_event(PRIVATE_EVENT_SCREEN_SETUP_MAIN);
            break;
        }
}
