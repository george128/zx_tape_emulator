#include <platform/utils.h>
#include <console/console.h>
#include <utils/strings.h>
#include <screens/setup/module_setup.h>
#include "module_main.h"
#include "screen_welcome.h"
#include "screen_files.h"
#include "screen_tap.h"
#include "screen_save.h"
#include "screen_file_ops.h"

PROGMEM static const char STR_SCR_MODULE_MAIN_NAME[]="main";
PROGMEM static const char STR_SCR_MODULE_SET_DATA[]="main: set data id=";
PROGMEM static const char STR_SCR_MODULE_GET_DATA[]="main: get data id=";

static char *path;

static PGM_P const screens_arr[] PROGMEM = {
    (PGM_P)&screen_welcome,
    (PGM_P)&screen_files,
    (PGM_P)&screen_tap,
    (PGM_P)&screen_save,
    (PGM_P)&screen_file_ops,
};

static void module_main_init()
{
    debug_puts("module_main_init()\n");
    path=0;
}

static void module_main_process_event(char event_id)
{
    switch(event_id) {
        case PUBLIC_EVENT_START:
            module_set_screen(SCREEN_WELCOME);
            break;
        case PUBLIC_EVENT_DONE:
        case PUBLIC_EVENT_RETURN:
        case PRIVATE_EVENT_SCREEN_WELCOME_DONE:
            module_set_screen(SCREEN_FILES);
            break;
        case PRIVATE_EVENT_TAP:
            module_set_screen(SCREEN_TAP);
            break;
        case PRIVATE_EVENT_SAVE_FILE:
            path=0;
            module_set_screen(SCREEN_SAVE);
            break;
        case PRIVATE_EVENT_FILE_OPS:
            module_set_screen(SCREEN_FILE_OPS);
            break;
        case PRIVATE_EVENT_SETUP:
            module_push(&module_setup);
            break;
        default:
            module_process_event_common(&module_main, event_id);
    }
}

static const struct screen *module_main_search_screen(char screen_id)
{
    return module_search_screen(screen_id, MAX_SCREEN_MODULE_MAIN,
                                &module_main,
                                screens_arr);
}

static void module_main_set_data(char data_id, void *ptr)
{
    debug_print("module_main_set_data id=%d ptr=%p\n",
                data_id,ptr);
    console_put_msg(STR_SCR_MODULE_SET_DATA, data_id);
    if(data_id == PRIVATE_DATA_EVENT_TAP_FILE) {
        path=ptr;
        if(path) {
            console_puts(path);
            console_next_line();
        }
    }
}

static void *module_main_get_data(char data_id)
{
    debug_print("module_main_get_data id=%d\n",
                data_id);
    console_put_msg(STR_SCR_MODULE_GET_DATA, data_id);
    if(data_id == PRIVATE_DATA_EVENT_TAP_FILE) {
        if(path != NULL) {
            console_puts(path);
            console_next_line();
        }
        return path;
    }
    return NULL;
}

PROGMEM const struct module module_main=
{
    .init=module_main_init,
    .process_event=module_main_process_event,
    .search_screen=module_main_search_screen,
    .get_data=module_main_get_data,
    .set_data=module_main_set_data,
    .name=STR_SCR_MODULE_MAIN_NAME,
};
