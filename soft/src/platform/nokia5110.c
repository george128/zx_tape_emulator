/* Nokia 5110 LCD AVR Library
 *
 * Copyright (C) 2015 Sergey Denisov.
 * Written by Sergey Denisov aka LittleBuster (DenisovS21@gmail.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public Licence
 * as published by the Free Software Foundation; either version 3
 * of the Licence, or (at your option) any later version.
 *
 * Original library written by SkewPL, http://skew.tk
 */

#include "nokia5110.h"

#include <avr/pgmspace.h>
#include <avr/io.h>
#include <util/delay.h>
#include <console/console.h>
#include <platform/settings.h>
#include "spi.h"
#include "utils.h"

// #define NOKIA_DEBUG

/*
 * LCD's port
 */
#define PORT_LCD PORTB
#define DDR_LCD DDRB

/*
 * LCD's pins
 */
#define LCD_SCE PB5
#define LCD_RST PB6
#define LCD_DC PB4

#ifdef NOKIA_DEBUG
PROGMEM static const char STR_NOKIA_CMD[]="Nokia LCD: cmd=";
PROGMEM static const char STR_NOKIA_RENDER_START[]="Nokia LCD: render start\n";
PROGMEM static const char STR_NOKIA_RENDER_DONE[]="Nokia LCD: render done\n";
PROGMEM static const char STR_NOKIA_CS_LOW[]="Nokia LCD: CS_LOW\n";
PROGMEM static const char STR_NOKIA_CS_HIGH[]="Nokia LCD: CS_HIGH\n";
#endif
PROGMEM static const char STR_NOKIA_BL[]="Nokia LCD: backight=";
PROGMEM static const char STR_NOKIA_ROTATE[]="Nokia LCD: rotate=";
PROGMEM static const char STR_NOKIA_INIT[]="Nokia LCD: init\n";
PROGMEM static const char STR_NOKIA_INIT_DONE[]="Nokia LCD: init done\n";

static unsigned char SPCR_init, SPSR_init;
static char display_rotate;

#define SPI_SAVE_REGS unsigned char SPCR_save=SPCR, SPSR_save=SPSR; \
\
SPCR=SPCR_init; \
SPSR=SPSR_init

#define SPI_RESTORE_REGS SPCR=SPCR_save; \
SPSR=SPSR_save

static void cs_low()
{
    PORT_LCD &= ~_BV(LCD_SCE);
#ifdef NOKIA_DEBUG
    console_puts_P(STR_NOKIA_CS_LOW);
#endif
}

static void cs_high()
{
    PORT_LCD |= _BV(LCD_SCE);
    _delay_us(50);
#ifdef NOKIA_DEBUG
    console_puts_P(STR_NOKIA_CS_HIGH);
#endif
}

/**
 * Sending data to LCD
 * @bytes: data
 * @is_data: transfer mode: 1 - data; 0 - command;
 */
static void write(uint8_t bytes, uint8_t is_data)
{
	/* We are sending data */
	if (is_data)
		PORT_LCD |= _BV(LCD_DC);
	/* We are sending commands */
	else
		PORT_LCD &= ~_BV(LCD_DC);

	/* Send bytes */
	spi_send_receive(bytes);

}

static void write_cmd(uint8_t cmd)
{
#ifdef NOKIA_DEBUG
    console_put_msg(STR_NOKIA_CMD, cmd);
#endif
    write(cmd, 0);
}

static void write_data(uint8_t data)
{
    write(data, 1);
}

void nokia_lcd_set_contrast(unsigned char lcd_contrast)
{
    SPI_SAVE_REGS;
    /* Enable controller */
    cs_low();

    /* -LCD Extended Commands mode- */
    write_cmd(0x21);
    write_cmd(0x80|lcd_contrast);
    /* Standard Commands mode, powered down */
    write_cmd(0x20);
    /* Disable controller */
    cs_high();
    SPI_RESTORE_REGS;
}

/*
 * Public functions
 */

void nokia_lcd_set_backlight(char bl)
{
    console_put_msg(STR_NOKIA_BL, bl);
    if(bl)
        PORTA |= _BV(0);
    else
        PORTA &= ~_BV(0);
}

void nokia_lcd_set_rotate(char rotate)
{
    display_rotate = rotate;
    console_put_msg(STR_NOKIA_ROTATE, rotate);
}

static void nokia_lcd_clear_internal(void)
{
    /* Set column and row to 0 */
    write_cmd(0x80);
    write_cmd(0x40);
}

void nokia_lcd_init(void)
{
    register short i;
    struct settings s;

    console_puts_P(STR_NOKIA_INIT);
    get_settings(&s);
    nokia_lcd_set_rotate(s.display_rotate);
    nokia_lcd_set_backlight(s.lcd_backlight);

    spi_init(0,0);

    SPCR_init=SPCR;
    SPSR_init=SPSR;

    /* Set pins as output */
    DDR_LCD |= _BV(LCD_SCE);
    DDR_LCD |= _BV(LCD_RST);
    DDR_LCD |= _BV(LCD_DC);
    DDRA |= _BV(0); /*LCD backlight*/

    /* Reset display */
    PORT_LCD |= _BV(LCD_RST);
    cs_high();
    _delay_ms(10);
    PORT_LCD &= ~_BV(LCD_RST);
    _delay_ms(70);
    PORT_LCD |= _BV(LCD_RST);

    /*
     * Initialize display
     */
    /* Enable controller */
    cs_low();
    /* -LCD Extended Commands mode- */
    write_cmd(0x21);
    /* LCD bias mode 1:48 */
    write_cmd(0x13);
    /* Set temperature coefficient */
    write_cmd(0x06);
    /* Default VOP (3.06 + 66 * 0.06 = 7V) */
    write_cmd(0x80|s.lcd_contrast);
    /* Standard Commands mode, powered down */
    write_cmd(0x20);
    /* LCD in normal mode */
    write_cmd(0x09);

    nokia_lcd_clear_internal();

    for (i = 0; i < 504; i++)
        write_data(0x00);

    /* Activate LCD */
    write_cmd(0x08);
    write_cmd(0x0C);
    cs_high();
    console_puts_P(STR_NOKIA_INIT_DONE);
}

void nokia_lcd_clear(void)
{
    SPI_SAVE_REGS;

    cs_low();

    nokia_lcd_clear_internal();
    SPI_RESTORE_REGS;

    cs_high();
}

void nokia_lcd_power(uint8_t on)
{
    SPI_SAVE_REGS;
    cs_low();

    write_cmd(on ? 0x20 : 0x24);

    cs_high();
    SPI_RESTORE_REGS;
}

void nokia_lcd_render(uint8_t *buf)
{
    unsigned short i;

    SPI_SAVE_REGS;
#ifdef NOKIA_DEBUG
    console_puts_P(STR_NOKIA_RENDER_START);
#endif
    cs_low();

    nokia_lcd_clear_internal();
    /* Write screen to display */
    if(display_rotate) {
        i=504;
        do {
            i--;
            uint8_t a=buf[i];
            BIT_REVERSE(a);
            write_data(a);
        } while(i!=0);
    }else{
        for (i = 0; i < 504; i++)
            write_data(*(buf++));
    }

    cs_high();
#ifdef NOKIA_DEBUG
    console_puts_P(STR_NOKIA_RENDER_DONE);
#endif
    SPI_RESTORE_REGS;
}
