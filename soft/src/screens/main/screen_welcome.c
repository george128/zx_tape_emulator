#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <graphics/graphics.h>
#include <timer/timer.h>
#include <keyboard/keyboard.h>
#include "module_main.h"
#include "screen_welcome.h"

struct data {
    struct timer t;
    unsigned char timer_id;
};

#define shm ((struct data *) (get_shared_mem()))

PROGMEM static const char STR_SCREEN_WELCOME_STARS[]  = "**************";
PROGMEM static const char STR_SCREEN_WELCOME_NAME[]   = " ZX Tape Emul";
PROGMEM static const char STR_SCREEN_WELCOME_VERSION[]= "     v" TAPE_VER;
PROGMEM static const char STR_SCREEN_WELCOME_SETUP[]  = "Left to setup";

static void screen_welcome_timer_callback(unsigned char timer_id, const void *unused)
{
    module_process_event(PRIVATE_EVENT_SCREEN_WELCOME_DONE);
}

static void screen_welcome_display() 
{
    clear_screen();
    draw_text(size_x(0),line_y(1),STR_SCREEN_WELCOME_STARS,0,1);
    draw_text(size_x(0),line_y(2),STR_SCREEN_WELCOME_NAME,0,1);
    draw_text(size_x(0),line_y(3),STR_SCREEN_WELCOME_VERSION,0,1);
    draw_text(size_x(0),line_y(4),STR_SCREEN_WELCOME_STARS,0,1);
    draw_text(size_x(0),line_y(5),STR_SCREEN_WELCOME_SETUP,0,1);
    flush_screen();

    shm->t.period=WELCOME_SHOW_TIME_MS;
    shm->t.callback=screen_welcome_timer_callback;
    shm->timer_id=timer_setup(&shm->t);
}

static void screen_welcome_destroy()
{
    timer_free(shm->timer_id);
}

static void screen_welcome_key_press(char keycode)
{
        switch(keycode) {
            case KEY_LEFT:
                module_process_event(PRIVATE_EVENT_SETUP);
                break;
        }
}

PROGMEM const struct screen screen_welcome=
{
    .display=screen_welcome_display,
    .destroy=screen_welcome_destroy,
    .key_press=screen_welcome_key_press,
    .shm_size=sizeof(struct timer)
};
