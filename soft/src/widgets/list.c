#include <graphics/graphics.h>

#include "list.h"

void g_list_init(struct g_list *data, unsigned char start_element) {
    data->current = start_element;
    if(data->is_vert) {
        data->highlighter_dim.pos.x=data->x_start;
        data->highlighter_dim.pos.y=data->y_start+start_element*LINE_HEIGHT;
    } else {
        data->highlighter_dim.pos.x=data->x_start+start_element*data->width;
        data->highlighter_dim.pos.y=data->y_start;
    }
    data->highlighter_dim.size.w=data->width;
    data->highlighter_dim.size.h=LINE_HEIGHT;
    g_list_redraw(data);
}

void g_list_redraw(struct g_list *data) {
    highlighter_set(&data->highlighter_dim);
    flush_screen();
}

void g_list_up(struct g_list *data) {
    if(data->current != 0) {
        data->current--;
        if(data->is_vert) {
            highlighter_move_y(&data->highlighter_dim,
                           data->highlighter_dim.pos.y-LINE_HEIGHT);
            data->highlighter_dim.pos.y-=LINE_HEIGHT;
        } else {
            highlighter_move_x(&data->highlighter_dim,
                           data->highlighter_dim.pos.x-data->width);
            data->highlighter_dim.pos.x-=data->width;
        }
    }
}

void g_list_down(struct g_list *data) {
    if(data->current+1 < data->num_elements) {
        data->current++;
        if(data->is_vert) {
            highlighter_move_y(&data->highlighter_dim,
                           data->highlighter_dim.pos.y+LINE_HEIGHT);
            data->highlighter_dim.pos.y+=LINE_HEIGHT;
        } else {
            highlighter_move_x(&data->highlighter_dim,
                           data->highlighter_dim.pos.x+data->width);
            data->highlighter_dim.pos.x+=data->width;
        }
    }
}

unsigned char g_list_get_current(struct g_list *data) {
    return data->current;
}
