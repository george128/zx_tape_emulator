#ifndef _SHARED_MEM_H_
#define _SHARED_MEM_H_

/*
shared memory for screens
values are freed on screen destroying
*/

#define SHARED_MEM_SIZE 4096

extern char screen_mem[SHARED_MEM_SIZE];

inline char *get_shared_mem(){return screen_mem;}

#endif /*_SHARED_MEM_H_*/
