#include "screen_setup_settings.h"

#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <screens/setup/module_setup.h>
#include <graphics/graphics.h>
#include <keyboard/keyboard.h>
#include <widgets/list.h>
#include <platform/utils.h>
#include <platform/settings.h>
#include <platform/nokia5110.h>

PROGMEM static const char STR_SCREEN_SETUP_SETTINGS_SOUND[]        ="Sound    :";
PROGMEM static const char STR_SCREEN_SETUP_SETTINGS_DISPLAY[]      ="Disp. rot:";
PROGMEM static const char STR_SCREEN_SETUP_SETTINGS_LCD_BACKLIGHT[]="LCD backl:";
PROGMEM static const char STR_SCREEN_SETUP_SETTINGS_ONOFF[]="On Off";

struct data {
    struct settings settings;
    struct g_list list;
    struct g_list list_onoff;
    char is_edit;
};

#define shm ((struct data *) (get_shared_mem()))

static void screen_setup_settings_redraw()
{
    char onoff[7];

    strncpy_P(onoff, STR_SCREEN_SETUP_SETTINGS_ONOFF, 7);
    onoff[2]=0;

    clear_screen();
    draw_text(size_x(0),line_y(1),STR_SCREEN_SETUP_SETTINGS_SOUND,0,1);
    draw_text(size_x(10),line_y(1),shm->settings.sound_on?onoff:onoff+3,0,0);
    draw_text(size_x(0),line_y(2),STR_SCREEN_SETUP_SETTINGS_DISPLAY,0,1);
    draw_text(size_x(10),line_y(2),shm->settings.display_rotate?onoff:onoff+3,0,0);
    draw_text(size_x(0),line_y(3),STR_SCREEN_SETUP_SETTINGS_LCD_BACKLIGHT,0,1);
    draw_text(size_x(10),line_y(3),shm->settings.lcd_backlight?onoff:onoff+3,0,0);
    if(shm->list.num_elements>0) {
        g_list_redraw(&shm->list);
    }
}

static void screen_setup_settings_display() 
{
    struct g_list *list = &shm->list;
    unsigned char x_start = size_x(0);
    unsigned char list_width = SCREEN_WIDTH - x_start - 1;

    get_settings(&shm->settings);
    shm->is_edit=0;
    list->num_elements=0;
    screen_setup_settings_redraw();

    list->num_elements=3;
    list->is_vert=1;
    list->width=list_width;
    list->x_start=x_start;
    list->y_start=line_y(1);
    g_list_init(list, 0);
}

static void screen_setup_settings_create_onoff()
{
    struct g_list *list = &shm->list_onoff;
    char val;
    draw_text(size_x(0),line_y(5),STR_SCREEN_SETUP_SETTINGS_ONOFF,0,1);
    
    switch(g_list_get_current(&shm->list)) {
        case 0:
            val=shm->settings.sound_on;
            break;
        case 1:
            val=shm->settings.display_rotate;
            break;
        case 2:
            val=shm->settings.lcd_backlight;
            break;
        default:
            val=0;
    }

    shm->is_edit=1;
    list->num_elements=2;
    list->is_vert=0;
    list->width=size_x(3);
    list->x_start=0;
    list->y_start=line_y(5);
    g_list_init(list, val?0:1);
}

static void screen_setup_settings_apply_choice()
{
    char val=g_list_get_current(&shm->list_onoff);
    val=val?0:1;
    
    switch(g_list_get_current(&shm->list)) {
        case 0:
            shm->settings.sound_on=val;
            break;
        case 1:
            shm->settings.display_rotate=val;
            nokia_lcd_set_rotate(val);
            break;
        case 2:
            shm->settings.lcd_backlight=val;
            nokia_lcd_set_backlight(val);
            break;
    }
}

static void screen_setup_settings_key_press(char keycode)
{
    switch(keycode) {
        case KEY_UP:
            if(!shm->is_edit)
                g_list_up(&shm->list);
            break;
        case KEY_DOWN:
            if(!shm->is_edit)
                g_list_down(&shm->list);
            break;
        case KEY_LEFT:
            if(shm->is_edit) {
                g_list_up(&shm->list_onoff);
                screen_setup_settings_apply_choice();
            }
            else 
                module_process_event(PRIVATE_EVENT_SCREEN_SETUP_MAIN);
            break;
        case KEY_RIGHT:
            if(shm->is_edit) {
                g_list_down(&shm->list_onoff);
                screen_setup_settings_apply_choice();
            }
            break;
        case KEY_ENTER:
            if(shm->is_edit) {
                save_settings(&shm->settings);
                shm->is_edit=0;
                screen_setup_settings_redraw();                
            } else {
                screen_setup_settings_create_onoff();
            }
            break;
    }


}

PROGMEM const struct screen screen_setup_settings =
{
    .display=screen_setup_settings_display,
    .destroy=NULL,
    .key_press=screen_setup_settings_key_press,
    .shm_size=sizeof(struct data)
};
