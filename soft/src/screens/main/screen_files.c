#include <console/console.h>
#include <keyboard/keyboard.h>
#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <graphics/graphics.h>
#include <widgets/list.h>
#include <disk/ff.h>
#include <disk/diskio.h>
#include <string.h>
#include "module_main.h"
#include "screen_files.h"

#define LIST_MAX 50
#define FNAME_MAX 20

#define list_width SCREEN_WIDTH


struct file_descr
{
    char fname[FNAME_MAX];
    unsigned char attr;
};

struct data {
    struct g_list list;
    unsigned char list_win_start;
    struct file_descr f[LIST_MAX];
    char num_files;
    char path[PATH_MAX];
};

#define shm ((struct data *) (get_shared_mem()))

PROGMEM static const char STR_READDIR_ERROR[]="readdir: error res=";
PROGMEM static const char STR_READDIR_TRUNCATE[]="readdir: truncate list files to ";
PROGMEM static const char STR_DIR_ERROR[]="Can't read SD";
PROGMEM static const char STR_NO_CARD[]="No SD Card";
PROGMEM static const char STR_DIR_PROCESS[]="Reading files";
PROGMEM static const char STR_DOTS[]="...";
PROGMEM static const char STR_MOUNT_RES[]="screen_file: mount res=";
PROGMEM static const char STR_FOUND_FILES[]="screen_file: found files=";
PROGMEM static const char STR_KEY[]="screen_file: key=";
PROGMEM static const char STR_KEY_UP[]="screen_file: keyUp win_start=";
PROGMEM static const char STR_KEY_DOWN[]="screen_file: keyDown win_start=";
PROGMEM static const char STR_CHK_LIST_UPDATE[]="screen_file: chk_list_update: unsupportd key:";
PROGMEM static const char STR_LIST_POS[]="list_pos=";
PROGMEM static const char STR_FILE_NAME_LEN[]="screen_file: file_len=";

static char read_dir()
{
    FATFS fatfs;
    DIR dir;
    FILINFO fno;
    char f_cnt=0;
    struct file_descr *f;
    size_t len;

    fatfs.drv=0; /*drive 0*/
    FRESULT res=f_mount(&fatfs,"0:",1);
    console_put_msg(STR_MOUNT_RES, res);

    if(res != FR_OK) {
        DSTATUS stat = disk_status(0);
        if(stat & STA_NODISK)
            return -2;
        else
            return -1;
    }
    res=f_opendir(&dir,shm->path);
    if(res != FR_OK) 
        goto l_umount;

    do {
        res=f_readdir(&dir,&fno);
        if (res != FR_OK) {
            console_put_msg(STR_READDIR_ERROR, res);
            break;
        }
        if(fno.fname[0] == 0)
            continue;
        f=&shm->f[f_cnt++];
        if((len=strlen(fno.fname))>=FNAME_MAX) {
            console_put_msg(STR_FILE_NAME_LEN, len);
            strncpy(f->fname,fno.fname,FNAME_MAX-5);
            strncpy(f->fname+FNAME_MAX-5,fno.fname+len-4,4);
        } else
            strncpy(f->fname,fno.fname,FNAME_MAX);

        f->fname[FNAME_MAX-1]=0;
        f->attr=fno.fattrib;
        
        if(f_cnt==LIST_MAX-1){
            console_put_msg(STR_READDIR_TRUNCATE,f_cnt);
            f=&shm->f[f_cnt];
            strncpy_P(f->fname, STR_DOTS,FNAME_MAX);
            f->attr=0x80;
            break;
        }
    } while(fno.fname[0] != 0);
    res=f_closedir(&dir);
 l_umount:
    res=f_mount(NULL,"0:",1); // umount
    {
        char c=0;
        disk_ioctl(0,CTRL_POWER, &c);
    } 
    return f_cnt;
}

static void screen_files_list_update()
{
    char i;
    struct file_descr *f;

    clear_screen();
    for(i=0;i<shm->list.num_elements;i++) {
        f=&shm->f[shm->list_win_start+i];
        if(f->attr&AM_DIR)
            draw_text(size_x(0),line_y(i),">",0,0);
        else if(f->attr&0x80)
            draw_text(size_x(0),line_y(i),".",0,0);
        draw_text(size_x(1),line_y(i),f->fname,0,0);
    }
    g_list_redraw(&shm->list);
}

static void screen_files_create_list()
{
    struct g_list *list = &shm->list;

    clear_screen();
    draw_text(0,line_y(3),STR_DIR_PROCESS,0,1);
    flush_screen();

    shm->list_win_start=0;
    shm->num_files=read_dir();
    console_put_msg(STR_FOUND_FILES,shm->num_files);
    if(shm->num_files==-1 || shm->num_files==-2) {
        clear_screen();
        if(shm->num_files==-1)
            draw_text(0,line_y(3),STR_DIR_ERROR,0,1);
        else
            draw_text(0,line_y(3),STR_NO_CARD,0,1);
        flush_screen();
        return;
    }

    list->num_elements=SCREEN_MAX_LINES>shm->num_files?shm->num_files:SCREEN_MAX_LINES;
    list->is_vert=1;
    list->width=list_width;
    list->x_start=0;
    list->y_start=line_y(0);
    g_list_init(list, 0);

    screen_files_list_update();
}

static void screen_files_display() 
{
    char *path=module_get_data(PRIVATE_DATA_EVENT_TAP_FILE);
    if(path==NULL) {
        shm->path[0]='/';
        shm->path[1]=0;
    } else {
        memmove(shm->path, path, PATH_MAX);
    }
    screen_files_create_list();
}

static void screen_files_destroy()
{
}

static char screen_files_chk_list_update(char keycode) 
{
    switch(keycode) {
	case KEY_UP:
	    return shm->list_win_start>0 
		&& g_list_get_current(&shm->list) == 0;
	case KEY_DOWN:
	    return shm->list_win_start < shm->num_files-shm->list.num_elements
		&& g_list_get_current(&shm->list)+1 == shm->list.num_elements;
    }
    console_put_msg(STR_CHK_LIST_UPDATE, keycode);
    return 0;
}

static char screen_files_can_append_path(char *s1, char *s2) {
    return strlen(s1)+strlen(s2)+1<PATH_MAX;
}

static char screen_files_push_path()
{
    struct file_descr *f=&shm->f[g_list_get_current(&shm->list)+shm->list_win_start];
    if(!screen_files_can_append_path(shm->path, f->fname)) 
        return 0;
    if(shm->path[1]!=0) {
        strcat(shm->path,"/");
    }
    strcat(shm->path,f->fname);
    module_set_data(PRIVATE_DATA_EVENT_TAP_FILE,shm->path);
    return 1;
}

static void screen_files_process_entry()
{
    struct file_descr *f=&shm->f[g_list_get_current(&shm->list)+shm->list_win_start];

    if(f->attr & 0x80)
	return;
    if(f->attr & AM_DIR) {
	if(screen_files_can_append_path(shm->path, f->fname)) {
        if(shm->path[1]!=0) {
            strcat(shm->path,"/");
        }
	    strcat(shm->path,f->fname);
	    screen_files_create_list();
	}
    } else {
        if(shm->num_files!=-1 && screen_files_push_path())
            module_process_event(PRIVATE_EVENT_TAP);
    }
}

static void screen_files_key_press(char keycode)
{
    console_put_msg(STR_KEY, keycode);
    switch(keycode) {
        case KEY_UP:
            console_puts_P(STR_KEY_UP);
            console_putd(shm->list_win_start);
            console_putc(' ');
            console_puts_P(STR_LIST_POS);
            console_putd(g_list_get_current(&shm->list));
            console_next_line();
            if(screen_files_chk_list_update(keycode)) {
                shm->list_win_start--;
                screen_files_list_update();
            } else 
                g_list_up(&shm->list);
            break;
        case KEY_DOWN:
            console_puts_P(STR_KEY_DOWN);
            console_putd(shm->list_win_start);
            console_putc(' ');
            console_puts_P(STR_LIST_POS);
            console_putd(g_list_get_current(&shm->list));
            console_next_line();
            if(screen_files_chk_list_update(keycode)) {
                shm->list_win_start++;
                screen_files_list_update();
            } else 
                g_list_down(&shm->list);
            break;
        case KEY_LEFT:
            if(strlen(shm->path)==2)
                break;
            {
                char *p=strrchr(shm->path,'/');
                *p=0;
                screen_files_create_list();
            }
            break;
        case KEY_RIGHT:
            if(shm->num_files!=-1 && screen_files_push_path())
                module_process_event(PRIVATE_EVENT_FILE_OPS);
            break;
        case KEY_ENTER:
            screen_files_process_entry();
            break;
    }
}

PROGMEM const struct screen screen_files=
{
    .display=screen_files_display,
    .destroy=screen_files_destroy,
    .key_press=screen_files_key_press,
    .shm_size=sizeof(struct data)
};
