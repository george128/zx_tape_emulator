#include <screens/shared_mem.h>
#include <platform/load_save_params.h>
#include <platform/utils.h>
#include <console/console.h>
#include "screen_setup_load.h"
#include "screen_setup_params_base.h"

struct data {
    struct params_data params_data;
    struct params_load params_load;
};

#define shm ((struct data *) (get_shared_mem()))

PROGMEM static const char STR_SCREEN_LOAD_PARAM_PILOT[]       = "Pilot :";
PROGMEM static const char STR_SCREEN_LOAD_PARAM_SYNC1[]       = "Sync1 :";
PROGMEM static const char STR_SCREEN_LOAD_PARAM_SYNC2[]       = "Sync2 :";
PROGMEM static const char STR_SCREEN_LOAD_PARAM_DATA1[]       = "Data1 :";
PROGMEM static const char STR_SCREEN_LOAD_PARAM_DATA0[]       = "Data0 :";
PROGMEM static const char STR_SCREEN_LOAD_PARAM_HEADER_DUR[]  = "HdrDur:";
PROGMEM static const char STR_SCREEN_LOAD_PARAM_DATA_DUR[]    = "DatDur:";
PROGMEM static const char STR_SCREEN_LOAD_PARAM_PAUSE[]       = "Pause :";

PROGMEM static const struct param_rec param_recs_load[] = {
    {
        .text=STR_SCREEN_LOAD_PARAM_PILOT,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_load, pilot),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_LOAD_PARAM_SYNC1,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_load, sync1),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_LOAD_PARAM_SYNC2,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_load, sync2),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_LOAD_PARAM_DATA1,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_load, data1),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_LOAD_PARAM_DATA0,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_load, data0),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_LOAD_PARAM_HEADER_DUR,
        .postfix=STR_POSTFIX_S,
        .offset=offsetof(struct params_load, duration_header),
        .to_str=params_char_to_str,
        .from_str=params_char_from_str,
    },
    {
        .text=STR_SCREEN_LOAD_PARAM_DATA_DUR,
        .postfix=STR_POSTFIX_S,
        .offset=offsetof(struct params_load, duration_data),
        .to_str=params_char_to_str,
        .from_str=params_char_from_str,
    },
    {
        .text=STR_SCREEN_LOAD_PARAM_PAUSE,
        .postfix=STR_POSTFIX_MS,
        .offset=offsetof(struct params_load, pause_block),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
    },
};

static void screen_setup_load_save_params(void *params)
{
    save_params_load(params);
}

static void screen_setup_load_get_params(void *params)
{
    get_params_load(params);
}

static void screen_setup_load_display()
{
    shm->params_data.save_params=screen_setup_load_save_params;
    shm->params_data.get_params=screen_setup_load_get_params;
    shm->params_data.params=&shm->params_load;
    screen_setup_params_init(&shm->params_data, param_recs_load, 8);
}

static void screen_setup_load_destroy()
{
}

static void screen_setup_load_key_press(char keycode)
{
    screen_setup_params_key_press(&shm->params_data, keycode);
}

PROGMEM const struct screen screen_setup_load=
{
    .display=screen_setup_load_display,
    .destroy=screen_setup_load_destroy,
    .key_press=screen_setup_load_key_press,
    .shm_size=sizeof(struct data)
};
