#include <string.h>
#include <graphics/graphics.h>
#include <platform/utils.h>
#include <keyboard/keyboard.h>
#include <console/console.h>

#include "highlighter.h"
#include "keyb.h"
/*                                                                                       bs  ret clr sav */
PROGMEM static const char STR_G_KEYB_ALPHA[]  = "0123456789abcdefhhigklmnopqrstuvwxyz-_.\x7f\x88\x8b\x8d";

/*                                                       bs  ret clr sav */
PROGMEM static const char STR_G_KEYB_DGT[]  = "0123456789\x7f\x88\x8b\x8d";

#define LINE_TEXT (SCREEN_MAX_LINES-1)

/* #define G_KEYB_DEBUG 1 */

#ifdef G_KEYB_DEBUG
PROGMEM static const char STR_G_KEYB_STATE1[]  = "keyb: cursor: {";
PROGMEM static const char STR_G_KEYB_BS[]  = "keyb: backspace";
PROGMEM static const char STR_G_KEYB_CLEAR[]  = "keyb: clear";
PROGMEM static const char STR_G_KEYB_SAVE[]  = "keyb: save";
PROGMEM static const char STR_G_KEYB_CANCEL[]  = "keyb: cancel";
PROGMEM static const char STR_G_KEYB_STATE2[]  = "} buf: pos=";
PROGMEM static const char STR_G_KEYB_STATE3[]  = " len=";
PROGMEM static const char STR_G_KEYB_STATE4[]  = " status=";

static void g_keyb_print_state(struct g_keyb *data) 
{
    console_puts_P(STR_G_KEYB_STATE1);
    console_putd(data->cursor_pos.x);
    console_putc(',');
    console_putd(data->cursor_pos.y);
    console_puts_P(STR_G_KEYB_STATE2);
    console_putd(data->buf_pos);
    console_puts_P(STR_G_KEYB_STATE3);
    console_putd(data->buf_len);
    console_puts_P(STR_G_KEYB_STATE4);
    console_putd(data->status);
    console_putc(' ');
    console_putc('\'');
    console_puts(data->buf);
    console_putc('\'');
    console_next_line();
}
#define g_keyb_puts_P(x) console_puts_P(x)
#else
#define g_keyb_print_state(x)
#define g_keyb_puts_P(x)
#endif

static void g_keyb_draw_text(struct g_keyb *data)
{
    clear_line(5);
    draw_text(size_x(0), line_y(LINE_TEXT), data->buf, 0, 0);
    invert_region(size_x(data->buf_pos),line_y(LINE_TEXT),size_x(data->buf_pos+1),line_y(5)+LINE_HEIGHT);
}

void g_keyb_init(struct g_keyb *data, char *buf, char buf_len, char type)
{
    clear_screen();
    data->chrs=type==G_KEYB_TYPE_DGT ? STR_G_KEYB_DGT : STR_G_KEYB_ALPHA;
    draw_text(size_x(0),line_y(0),data->chrs,1,1);
    data->highlighter_dim.pos.x=0;
    data->highlighter_dim.pos.y=0;
    data->highlighter_dim.size.w=CHAR_WIDTH;
    data->highlighter_dim.size.h=LINE_HEIGHT;

    data->status=G_KEYB_EDIT;
    data->cursor_pos.x=data->cursor_pos.y=0;
    data->buf_pos=strlen(buf);
    if(data->buf_pos>=SCREEN_WIDTH_CHARS)
        data->buf_pos=SCREEN_WIDTH_CHARS-1;
    data->buf=buf;
    data->buf_len=buf_len;

    highlighter_set(&data->highlighter_dim);
    g_keyb_draw_text(data);
    g_keyb_print_state(data);
    flush_screen();
}

static void g_keyb_process_selected(struct g_keyb *data, unsigned char ch) {
    switch(ch) {
        case 0x7f: //backspace
            g_keyb_puts_P(STR_G_KEYB_BS);
            if(data->buf_pos>=0) {
                data->buf[--data->buf_pos]=0;
                g_keyb_draw_text(data);
                flush_screen();
            }
            break;
        case 0x88: //cancel & return
            g_keyb_puts_P(STR_G_KEYB_CANCEL);
            data->status=G_KEYB_CANCEL;
            break;
        case 0x8b: //clear
            g_keyb_puts_P(STR_G_KEYB_CLEAR);
            data->buf[0]=0;
            data->buf_pos=0;
            g_keyb_draw_text(data);
            flush_screen();
            break;
        case 0x8d: //save & return
            g_keyb_puts_P(STR_G_KEYB_SAVE);
            data->status=G_KEYB_DONE;
            break;
        default:
            if(data->buf_pos<data->buf_len-1) {
                char pch=data->buf[data->buf_pos];
                data->buf[data->buf_pos++]=ch;
                if(pch==0)
                    data->buf[data->buf_pos]=0;
                g_keyb_draw_text(data);
                flush_screen();
            }
    }
}

static char get_alpha_len(const char *chrs) {
    char i=0, ch;
    do {
        ch=pgm_read_byte(&chrs[i++]);
    } while(ch);
    return i-1;
}

char g_keyb_process_key(struct g_keyb *data, char keycode)
{
    char alpha_len=get_alpha_len(data->chrs);
    char max_y=alpha_len/SCREEN_WIDTH_CHARS;
    char max_last_x=alpha_len-max_y*SCREEN_WIDTH_CHARS;

#if G_KEYB_DEBUG>1
    console_putd(alpha_len);
    console_putc(' ');
    console_putd(max_y);
    console_putc(' ');
    console_putd(max_last_x);
    console_next_line();
#endif
    switch(keycode) {
        case KEY_UP:
            if(data->cursor_pos.y>0) {
                highlighter_move_y(&data->highlighter_dim,data->highlighter_dim.pos.y-LINE_HEIGHT);
                data->highlighter_dim.pos.y-=LINE_HEIGHT;
                data->cursor_pos.y--;
            }
            break;
        case KEY_DOWN:
            if(data->cursor_pos.y<max_y && (data->cursor_pos.y!=max_y-1||data->cursor_pos.x<max_last_x)) {
                highlighter_move_y(&data->highlighter_dim,data->highlighter_dim.pos.y+LINE_HEIGHT);
                data->highlighter_dim.pos.y+=LINE_HEIGHT;
                data->cursor_pos.y++;
            }
            break;
        case KEY_LEFT:
            if(data->cursor_pos.x>0) {
                highlighter_move_x(&data->highlighter_dim,data->highlighter_dim.pos.x-CHAR_WIDTH);
                data->highlighter_dim.pos.x-=CHAR_WIDTH;
                data->cursor_pos.x--;
            }
            break;
        case KEY_RIGHT:
            if(data->cursor_pos.x<SCREEN_WIDTH_CHARS && (data->cursor_pos.y!=max_y || data->cursor_pos.x+1!=max_last_x) ) {
                highlighter_move_x(&data->highlighter_dim,data->highlighter_dim.pos.x+CHAR_WIDTH);
                data->highlighter_dim.pos.x+=CHAR_WIDTH;
                data->cursor_pos.x++;
            }
            break;
        case KEY_ENTER:
            g_keyb_process_selected(data,pgm_read_byte(&data->chrs[data->cursor_pos.x+data->cursor_pos.y*SCREEN_WIDTH_CHARS]));
            break;
    }
    g_keyb_print_state(data);
    return data->status;
}

char g_keyb_status(struct g_keyb *data)
{
    return data->status;
}
