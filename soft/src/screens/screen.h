#ifndef _SCREEN_H_
#define _SCREEN_H_

struct screen
{
    void (*display)();
    void (*destroy)();
    void (*key_press)(char keycode);
    unsigned short shm_size;
};

#endif /*_SCREEN_H_*/
