#ifndef _SETTINGS_H_
#define _SETTINGS_H_

struct settings {
    char sound_on;
    char display_rotate;
    unsigned char lcd_contrast;
    char lcd_backlight;
};

void get_settings(struct settings *s);
void save_settings(struct settings *s);

#endif /*_SETTINGS_H_*/
