#include <avr/io.h>

#include "adc.h"

#define ADC_PIN PIN7

void adc_init()
{
    DDRF&=_BV(PIN7);
    /*disable ADC7 pin input*/
    DIDR0=_BV(ADC7D);
    /*ref=AVCC, left adj. res., mux=ADC7(000111)*/
    ADMUX=_BV(REFS0)|_BV(ADLAR)|_BV(MUX2)|_BV(MUX1)|_BV(MUX0);
    /*adc enable, prescaler=16*/
    ADCSRA=_BV(ADEN)|_BV(ADPS2);
    ADCSRB=0;
}

void adc_stop()
{
    /*Disable ADC*/
    ADCSRA&=_BV(ADEN);
}

unsigned char adc_get_value()
{
    ADCSRA|=_BV(ADSC);
    while(ADCSRA&_BV(ADSC));
    return ADCH;
}
