#include <disk/ff.h>
#include <disk/diskio.h>
#include <console/console.h>
#include <platform/utils.h>
#include <utils/strings.h>

PROGMEM static const char STR_STATUS[]="status: ";
PROGMEM static const char STR_INIT[]="init: ";
PROGMEM static const char STR_ERROR[]="Error!!!\n";
PROGMEM static const char STR_MMC_TYPE[]="mmc type: ";
PROGMEM static const char STR_MMC_TYPE_RES[]="mmc type res: ";
PROGMEM static const char STR_MMC_CSD[]="mmc csd: ";
PROGMEM static const char STR_MMC_CSD_RES[]="mmc csd res: ";
PROGMEM static const char STR_MMC_CID[]="mmc cid: ";
PROGMEM static const char STR_MMC_CID_RES[]="mmc cid res: ";
PROGMEM static const char STR_MMC_OCR[]="mmc ocr: ";
PROGMEM static const char STR_MMC_OCR_RES[]="mmc ocr res: ";
PROGMEM static const char STR_DISK_READ[]="disk_read: ";
PROGMEM static const char STR_BUF[]="buf: '";
PROGMEM static const char STR_DISKIO_TEST_DONE[]="diskio test done\n";
PROGMEM static const char STR_DISKIO_TEST2[]="diskio_test2\n";
PROGMEM static const char STR_DISKIO_TEST2_DONE[]="diskio_test2 done\n";
PROGMEM static const char STR_MOUNT[]="mount: ";
PROGMEM static const char STR_FATFS[]="fatfs: ";
PROGMEM static const char STR_OPEN[]="open: ";
PROGMEM static const char STR_FLAG[]=" flag=";
PROGMEM static const char STR_FIL[]="fil: ";
PROGMEM static const char STR_READ_RES[]="read: res=";
PROGMEM static const char STR_RB[]="rb=";
PROGMEM static const char STR_CLOSE_RES[]="close res=";
PROGMEM static const char STR_UMOUNT_RES[]="umount res=";

PROGMEM static const char STR_OPENDIR_RES[]="opendir: res=";
PROGMEM static const char STR_READDIR_RES[]="readdir: res=";
PROGMEM static const char STR_READDIR_END[]="readdir: end\n";


static void print_res(const char *str, unsigned char res) {
    console_puts_P(str);
    console_putd(res);
    console_putc('\n');
}

static void print_status() 
{
    print_res(STR_STATUS, disk_status(0));
}

static inline void disk_test1()
{
    unsigned char res;
    char buf[512];

    print_status();
    res=disk_initialize(0);
    print_res(STR_INIT, res);
    print_status();

    res=disk_ioctl(0, MMC_GET_TYPE, buf);
    print_res(STR_MMC_TYPE_RES, res);
    print_res(STR_MMC_TYPE, buf[0]);

    res=disk_ioctl(0, MMC_GET_CSD, buf);
    print_res(STR_MMC_CSD_RES, res);
    console_puts_P(STR_MMC_CSD);
    console_putmem(buf, 16);
    console_putc('\n');

    res=disk_ioctl(0, MMC_GET_CID, buf);
    print_res(STR_MMC_CID_RES, res);
    console_puts_P(STR_MMC_CID);
    console_putmem(buf, 16);
    console_putc('\n');

    res=disk_ioctl(0, MMC_GET_OCR, buf);
    print_res(STR_MMC_OCR_RES, res);
    console_puts_P(STR_MMC_OCR);
    console_putmem(buf, 4);
    console_putc('\n');

    res=disk_read(0, (BYTE *)buf, 2152, 1); /*test.txt contents*/
    buf[511]=0;
    print_res(STR_DISK_READ, res);
    console_puts_P(STR_BUF);
    console_puts(buf);
    console_putc('\'');
    console_putc('\n');

    console_puts_P(STR_DISKIO_TEST_DONE);
}


static inline void disk_test2()
{
    char buf[256];
    char path[256]="/";
    FATFS fatfs;
    FIL fil;
    UINT rb;
    DIR dir;
    FILINFO fno;
    

    fatfs.drv=0; /*drive 0*/
    console_puts_P(STR_DISKIO_TEST2);

    FRESULT res=f_mount(&fatfs,"/",1);
    console_puts_P(STR_MOUNT);
    console_putd(res);
    console_putc('\n');
    print_status();
    if(res!=FR_OK) goto err;
    
    console_puts_P(STR_FATFS);
    console_putmem(&fatfs, (char)sizeof(fatfs));
    console_putc('\n');

    res = f_opendir(&dir, path);
    console_put_msg(STR_OPENDIR_RES, res);
    if(res!=FR_OK) goto err;
    
    for (;;) {
        res = f_readdir(&dir, &fno);                   /* Read a directory item */
	console_put_msg(STR_READDIR_RES, res);
        if (res != FR_OK || fno.fname[0] == 0) {
	    console_puts_P(STR_READDIR_END);
            break;  /* Break on error or end of dir */
	}
	
        if (fno.fattrib & AM_DIR) {                    /* It is a directory */
	    console_putc('D');
        } else {                                       /* It is a file. */
            console_putc('F');
        }
        console_putc(':');
        console_puts(path);
        console_puts(fno.fname);
        console_next_line();
    }
    f_closedir(&dir);

    res=f_open(&fil,"/test.txt", FA_READ);
    console_puts_P(STR_OPEN);
    console_putd(res);
    console_puts_P(STR_FLAG);
    console_putd(fil.flag);
    console_putc('\n');
    console_puts_P(STR_FIL);
    console_putmem(&fil, sizeof(fil));
    console_putc('\n');
    print_status();
    if(res!=FR_OK) goto err_umount;

    res=f_read(&fil,buf,64,&rb);
    buf[rb]=0;
    console_puts_P(STR_READ_RES);
    console_putd(res);
    console_putc('\n');
    console_puts_P(STR_RB);
    console_putd(rb);
    console_putc('\n');
    print_status();
    if(res!=FR_OK) goto err_close;

    console_puts_P(STR_BUF);
    console_puts(buf);
    console_putc('\'');
    console_putc('\n');

err_close:
    res=f_close(&fil);
    console_puts_P(STR_CLOSE_RES);
    console_putd(res);
    console_putc('\n');
    //if(res!=FR_OK) goto err;

err_umount:
    res=f_mount(NULL,"/",1); // umount
    console_puts_P(STR_UMOUNT_RES);
    console_putd(res);
    console_putc('\n');
    if(res!=FR_OK) goto err;

    console_puts_P(STR_DISKIO_TEST2_DONE);
    return;
err:
    console_puts_P(STR_ERROR);
}

void disk_test()
{
    disk_test2();
}
