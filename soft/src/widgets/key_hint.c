#include <graphics/graphics.h>
#include <utils/sleep.h>

#include "key_hint.h"

void key_hint_display(struct key_hint *data) {
    unsigned char i;
    for(i=0;i<4;i++) {
        draw_char(size_x(0), line_y(i*2), data->left_chars[i]);
        draw_char(right_align_x(1) + 1, line_y(i*2), data->right_chars[i]);
    }
    draw_line(size_x(1) - 1, 0, size_x(1) - 1, SCREEN_HEIGHT);
    draw_line(right_align_x(1), 0, right_align_x(1), SCREEN_HEIGHT);
}



void key_hint_key_press(unsigned char is_right, unsigned char key_num) {
    unsigned char x1 = is_right? right_align_x(1): size_x(0);
    unsigned char y1 = line_y(key_num*2);
    unsigned char x2 = x1+CHAR_WIDTH;
    unsigned char y2 = y1+LINE_HEIGHT;

    invert_region(x1, y1, x2, y2);
    flush_screen();
    sleep_ms100(1);
    invert_region(x1, y1, x2, y2);
    flush_screen();
}
