#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include "font5x8.h"

#define GRAPHICS_DEBUG 1

#define CONTOLLER_WIDTH 84
#define SCREEN_WIDTH CONTOLLER_WIDTH
#define SCREEN_HEIGHT 48

#define SCREEN_MAX_LINES SCREEN_HEIGHT/FONT_HEIGH

#define LINE_HEIGHT FONT_HEIGH
#define CHAR_WIDTH (FONT_WIDTH+1)

#define SCREEN_WIDTH_CHARS (SCREEN_WIDTH/CHAR_WIDTH)

void graphics_init();

#if GRAPHICS_DEBUG > 2
void graphics_test();
#endif

void graphics_flush_auto(char is_flush);

void set_pos(char x, char y);
void draw_pixel(char x, char y, char is_set);
void draw_line(char x1, char y1, char x2, char y2);
void draw_char(char x, char y, const char c);
void draw_text(char x, char y, const char *text, char is_wrap, char is_progmem);
void draw_pic(char x, char y, char width, char height, char *data);
void clear_screen();
void flush_screen();
void invert_region(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2);
void clear_region(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2);

#define right_align_x(len) (SCREEN_WIDTH-CHAR_WIDTH*(len)-1)
#define center_align_x(len) ((SCREEN_WIDTH-CHAR_WIDTH*(len))/2-1)
#define size_x(len) (CHAR_WIDTH*(len))
#define line_y(line) (LINE_HEIGHT*(line))
#define bottom_line_y() (LINE_HEIGHT*(SCREEN_MAX_LINES-1))
#define clear_line(line) clear_region(0, line_y(line), SCREEN_WIDTH, line_y(line+1)-1)
#define clear_lines(line1, line2) clear_region(0, line_y(line), SCREEN_WIDTH, line_y(line2+1)-1)
#define clear_line_part(line, pos_x1, pos_x2) clear_region(size_x(pos_x1), line_y(line), size_x(pos_x2), line_y(line+1)-1)

#endif /*_GRAPHICS_H_*/
