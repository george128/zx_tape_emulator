#ifndef _TAP_H_
#define _TAP_H_

struct tap_header {
    unsigned char flag; /*00-headers FF-data*/
    unsigned char type; /*0-Prog,1-NumArr,2-ChrArr,3-code*/
    char name[10];
    unsigned short data_len;
    unsigned short start_line;
    unsigned short program_len;
    unsigned char chksum;
} __attribute__((packed));

struct tap_handler {
    /*provide next portion of data*/
    short (*get_next_data)(char *buf, short len);
    void (*found_header)(struct tap_header *h, unsigned long file_pos);
    void (*found_block)(unsigned short len, unsigned long file_pos);
    void (*log)(const char *format,...);
};

#define TAP_BUF_SIZE 256

#define TAP_STATE_DONE 0
#define TAP_STATE_PROCESS 1
#define TAP_STATE_ERR -1

struct tap_state {
    char buf[TAP_BUF_SIZE];
    short buf_len;
    char is_first;
    unsigned short block_len;
    unsigned short block_pos;
    unsigned long file_pos;
};

char tap_process(const struct tap_handler *handler, struct tap_state *state);

#endif /*_TAP_H_*/
