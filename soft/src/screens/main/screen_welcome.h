#ifndef _SCREEN_WELCOME_H_
#define _SCREEN_WELCOME_H_

#include <screens/screen.h>

#define WELCOME_SHOW_TIME_MS 1000
#define TAPE_VER "1.0"
extern const struct screen screen_welcome;

#endif /*_SCREEN_WELCOME_H_*/
