#ifndef _PLATFORM_UTILS_H_
#define _PLATFORM_UTILS_H_

#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>

#define read_rom(a) pgm_read_byte(a)

#define debug_print(a, ...) 
#define debug_puts(a)

#ifndef NULL
#define NULL ((void*)0)
#endif /*NULL*/

#define eeprom_register_block(src, size) 

#define BIT_REVERSE(a) do                 \
{                                         \
  a=((a>>1)&0x55)|((a<<1)&0xaa);          \
  a=((a>>2)&0x33)|((a<<2)&0xcc);          \
  a=((a>>4)&0x0f)|((a<<4)&0xf0);          \
} while(0)

#endif /*_PLATFORM_UTILS_H_*/
