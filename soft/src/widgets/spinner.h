#ifndef _SPINNER_H_
#define _SPINNER_H_

#include "widget.h"

struct g_spinner {
    char type;
    char is_selected;
    struct pos start_pos;
    union {
        struct {
            union {
                char **opts_ram;
                PGM_P const *opts_pgm;
            } type;
            char opts_max;
        } str;
        struct {
            short min;
            short max;
            char min_len;
        } dgt;
    } opts;
    char max_chars;
    short cur_pos;
};

void g_spinner_setup_dgt(char start_x, char start_y, short min, short max, 
                         char min_len, short pos, struct g_spinner *spinner);
void g_spinner_setup_str_mem(char start_x, char start_y, char *opts[], 
                       char opts_max, short pos, struct g_spinner *spinner);
void g_spinner_setup_str_pgm(char start_x, char start_y, PGM_P const opts[], 
                       char opts_max, short pos, struct g_spinner *spinner);
char g_spinner_set_pos(short pos, struct g_spinner *spinner);
void g_spinner_set_selected(struct g_spinner *spinner, char is_selected);
void g_spinner_redraw(struct g_spinner *spinner);
char g_spinner_up(struct g_spinner *spinner);
char g_spinner_down(struct g_spinner *spinner);
short g_spinner_get_cur_pos(struct g_spinner *spinner);
#endif /*_SPINNER_H_*/
