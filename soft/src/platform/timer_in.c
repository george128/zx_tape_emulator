#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <platform/utils.h>
#include <platform/settings.h>
#include <console/console.h>

#include "timer_in.h"

/*PL0 - input capture pin*/
#define PIN_BUZ _BV(4)

/*
#define PILOT_MAX 13714 //3000
#define PILOT_MIN 6857 //1500
#define SYNC1_MAX 5028 //1100
#define SYNC_TOTAL_MAX 16430 //3594
//#define ZERO_THRESHOLD 10971 //2400
#define ZERO_THRESHOLD 10057 //2200
#define DATA_TOTAL_MAX 23058 //5044
#define MIN_PILOT_COUNT 256
*/
#define STATE_BEGIN 0
#define STATE_PILOT 1
#define STATE_SYN1 2
#define STATE_SYN2 3
#define STATE_DATA 4
#define STATE_FINISH 5
#define STATE_OVERFLOW 7

#define BUF_STATE_FREE 0
#define BUF_STATE_FULL 1
#define BUF_STATE_PROCESS 2


PROGMEM static const char STR_STATE[]  = "state: ";
PROGMEM static const char STR_BUF[]  = "buf[";
PROGMEM static const char STR_BUF_LEN[]  = "] len=";

static struct timer_in_state *state;

static inline void set_state(char new_state) 
{
    state->state=new_state;
    PORTK=new_state;
}

static void tape_in_timer_callback(unsigned char timer_id, const void *data) 
{
    struct inbuf *ib;
    struct timer_in_handler *handler_p = (struct timer_in_handler *)data, h;
    char idx,buf_no=state->last_buf_no==0?1:0;
    short wb;

    do {
        if(state->last_state!=state->state) {
            console_puts_P(STR_STATE);
            console_putd(state->state);
            console_putc(' ');
            console_put_short(state->buf_pos);
            console_putc(' ');
            console_putd(state->buf_no);
            console_putc(' ');
            console_putd(state->last_buf_no);
            console_putc(' ');
            console_putd(state->inbuf[0].flags);
            console_putc(' ');
            console_putd(state->inbuf[1].flags);
            console_putc(' ');
            console_put_short(state->pilot_cnt);
            console_next_line();
            state->last_state=state->state;
        }
    
        if(state->inbuf[buf_no].flags==BUF_STATE_FULL) {
            idx=buf_no;
            ib=&state->inbuf[idx];
        } else if(state->inbuf[buf_no==0?1:0].flags==BUF_STATE_FULL) { 
            idx=buf_no==0?1:0;
            ib=&state->inbuf[idx];
        } else {
            return;
        }
        state->last_buf_no=idx;

        console_puts_P(STR_BUF);
        console_putd(idx);
        console_puts_P(STR_BUF_LEN);
        console_put_short(ib->len);
        console_next_line();
        /*for(i=0;i<ib->len;i+=127) {
            console_putmem(ib->buf+i, ib->len-i>127?127:ib->len-i);
            console_putc(' ');
        }
        console_next_line();
        */
        memcpy_P(&h,handler_p,sizeof(h));
        
        wb=h.save_next_data(ib->buf, ib->len, ib->is_first, ib->is_last);
        if(ib->is_last && ib->is_first && ib->len==sizeof(struct tap_header) && ib->buf[0]==0) {
            h.found_header((struct tap_header *)ib->buf);
        }
        ib->len=0;
        ib->flags=BUF_STATE_FREE;
        if(wb<0)
            return;
    } while(1);
}

void timer_in_init(struct timer_in_state *state_init) 
{
    struct settings s;

    state=state_init;
    get_params_save(&state->params_save);
    get_settings(&s);
    state->sound_on = s.sound_on;
    DDRL&=~_BV(0);
    PORTL&=~_BV(0);
    DDRE|=_BV(4);
    DDRK=0xff;
}

void timer_in_start(const struct timer_in_handler *handler)
{
    state->bit_pos=state->buf_no=0;
    state->last_buf_no=1;
    set_state(STATE_BEGIN);
    state->last_state=state->state;

    state->timer_ovf=0;
    state->cnt_prev=0;
    state->inbuf[0].flags=state->inbuf[1].flags=BUF_STATE_FREE;
    state->pilot_cnt=0;
    TCNT4=0;
    TCCR4A=0; /*no Compare Output mode */
    TCCR4B=_BV(CS40)|_BV(ICNC4)|_BV(ICES4); /*MODE0 (Normal), CLKio/1, rising edge input trigger*/
    TCCR4C=0; /*no force output compare*/
    TIMSK4=_BV(ICIE4)|_BV(TOIE4);

    state->t.period=50;
    state->t.callback=tape_in_timer_callback;
    state->t.data=handler;
    state->timer_id=timer_setup(&state->t);
}

void timer_in_done()
{
    TCCR4B=0; /*stop timer*/
    TIMSK4=0; /*disable timer interrupts*/
    if(state->timer_id>0)
        timer_free(state->timer_id);
    state=NULL;
}

static char tape_in_get_new_buf_no()
{
    char new_buf_no = state->buf_no==0?1:0;
    if(state->inbuf[new_buf_no].flags!=BUF_STATE_FREE)
        return 0x7f;
    return new_buf_no;
}

static void tape_in_add_bit(char bit)
{
    struct inbuf *ib=&state->inbuf[state->buf_no];
    char *b=ib->buf+state->buf_pos;

    *b<<=1;
    if(bit)
        (*b)++;
    if(++state->bit_pos == 8) {
        state->bit_pos=0;
        // console_putd(*b);
        // console_putc(' ');
        if(++state->buf_pos==TAPE_IN_BUF_SIZE) {
            char new_buf_no = tape_in_get_new_buf_no();
            ib->flags=BUF_STATE_FULL;
            ib->len=state->buf_pos;
            ib->is_last=0;
            /*
            console_putc('N');
            console_putd(new_buf_no);
            console_putc(' ');
            console_put_short(ib->len);
            console_next_line();
            */
            if(new_buf_no == 0x7f) {
                set_state(STATE_OVERFLOW);
                return;
            }
            ib=&state->inbuf[new_buf_no];
            state->buf_pos=0;
            state->buf_no=new_buf_no;
            ib->is_first=0;
            ib->is_last=0;
            ib->flags=BUF_STATE_PROCESS;
        }
    }
}

static unsigned short timer_in_get_period(unsigned short cnt_cur) 
{
    unsigned short period;
    switch(state->timer_ovf) {
        case 0:
            period=cnt_cur-state->cnt_prev;
            break;
        case 1:
            period=0xffff-state->cnt_prev+cnt_cur;
            break;
        default:
            period=0xffff;
    }
    state->cnt_prev=cnt_cur;
    state->timer_ovf=0;
    return period;
}

ISR(TIMER4_OVF_vect)
{
    if(++state->timer_ovf==3) {
        state->timer_ovf=2;
        if(state->state!=STATE_BEGIN) {
            console_putc('O');
            console_putc(' ');
            console_put_short(state->buf_pos);
            console_next_line();
        }
        if(state->state!=STATE_OVERFLOW)
            set_state(STATE_BEGIN);
    } else if(state->state==STATE_DATA && state->timer_ovf==2) {
        console_putc('F');
        console_putc(' ');
        console_put_short(state->buf_pos);
        console_next_line();
        state->inbuf[state->buf_no].len=state->buf_pos;
        state->inbuf[state->buf_no].flags=BUF_STATE_FULL;
        state->inbuf[state->buf_no].is_last=1;
        char new_buf_no=tape_in_get_new_buf_no();
        if(new_buf_no != 0x7f) {
            state->buf_no=new_buf_no;
            state->buf_pos=0;
            state->bit_pos=0;
        }
        set_state(STATE_BEGIN);
    }
}

ISR(TIMER4_CAPT_vect)
{
    unsigned short period=timer_in_get_period(ICR4);

    if(state->sound_on)
        PORTE|=_BV(PIN_BUZ);

    TCCR4B^=_BV(ICES4); /*change trigger edge*/

    switch(state->state) {
        case STATE_BEGIN:
            if(period > state->params_save.pilot_min && period < state->params_save.pilot_max) {
                state->pilot_cnt++;
                if(state->pilot_cnt >= state->params_save.min_pilot_count) {
                    set_state(STATE_PILOT);
                }
            } else {
                state->pilot_cnt=0;
            }
            break;
        case STATE_PILOT:
            if(period > state->params_save.sync_total_max) {
                state->pilot_cnt=0;
                set_state(STATE_BEGIN);
            } else if(period < state->params_save.sync1_max) {
                set_state(STATE_SYN1);
                state->pilot_cnt=0;
                state->period_prev = period;
            } else {
                state->pilot_cnt++;
            }
            break;
        case STATE_SYN1:
            if(state->period_prev+period < state->params_save.sync_total_max) {
                set_state(STATE_SYN2);
            } else {
                set_state(STATE_BEGIN);
            }
            state->pilot_cnt=0;
            break;
        case STATE_SYN2:
            state->period_prev=period;
            state->buf_pos=state->bit_pos=0;
            state->inbuf[state->buf_no].is_first=1;
            state->inbuf[state->buf_no].flags=BUF_STATE_PROCESS;
            set_state(STATE_DATA);
            break;
        case STATE_DATA:
            if(state->period_prev==0) {
                state->period_prev=period;
                break;
            }
            if(state->period_prev+period<state->params_save.zero_threshold) {
                tape_in_add_bit(0);
            } else if(state->period_prev+period>state->params_save.data_total_max) {
                state->inbuf[state->buf_no].len=state->buf_pos;
                state->buf_pos=state->bit_pos=0;
                if(state->buf_pos != 0) {
                    state->inbuf[state->buf_no].flags=BUF_STATE_FULL;
                    state->inbuf[state->buf_no].is_last=1;
                    char new_buf_no=tape_in_get_new_buf_no();
                    if(new_buf_no != 0x7f) {
                        state->buf_no=new_buf_no;
                        state->inbuf[state->buf_no].is_first=0;
                        state->inbuf[state->buf_no].is_last=0;
                    }
                }
                console_putc('D');
                console_putc(' ');
                console_put_short(state->buf_pos);
                console_next_line();
                set_state(STATE_BEGIN);
            } else {
                tape_in_add_bit(1);
            }
            state->period_prev=0;
            break;
    }
    if(state->sound_on)
        PORTE&=~_BV(PIN_BUZ);
}
