#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

enum KEYCODE {
    KEY_0=0,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7
};

#define KEY_UP KEY_3
#define KEY_DOWN KEY_5
#define KEY_LEFT KEY_2
#define KEY_RIGHT KEY_6
//#define KEY_RETURN KEY_
#define KEY_ENTER KEY_4
//#define KEY_CANCEL KEY_
#define KEY_BS 0x7F

void keyb_init();

/* if a key was pressed*/
char kbhit();

/* return keycode or blocked until key will be pressed */
char get_keycode();

#endif /*_KEYBOARD_H_*/
