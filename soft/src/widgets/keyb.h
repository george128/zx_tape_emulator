#ifndef _G_KEYB_H_
#define _G_KEYB_H_

#include "widget.h"
#include "highlighter.h"

#define G_KEYB_EDIT 0
#define G_KEYB_DONE 1
#define G_KEYB_CANCEL 2

#define G_KEYB_TYPE_ALPHA 0
#define G_KEYB_TYPE_DGT 1

struct g_keyb {
    struct pos cursor_pos;
    struct dim highlighter_dim;
    unsigned char buf_pos, buf_len;
    char *buf;
    char status;
    const char *chrs;
};

void g_keyb_init(struct g_keyb *data, char *buf, char buf_len, char type);
char g_keyb_process_key(struct g_keyb *data, char keycode);
char g_keyb_status(struct g_keyb *data);
#endif /*_G_KEYB_H_*/
