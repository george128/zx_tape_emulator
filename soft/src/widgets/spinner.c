#include <platform/utils.h>
#include <utils/strconv.h>
#include <utils/strings.h>
#include <graphics/graphics.h>
#include <utils/strings.h>
#include <string.h>

#include "spinner.h"

#define SPINNER_TYPE_STR_RAM 0
#define SPINNER_TYPE_STR_PGM 1
#define SPINNER_TYPE_DGT 2


static void get_str(char *buf, struct g_spinner *spinner) {
    switch(spinner->type) {
        case SPINNER_TYPE_DGT:
            short2dec(spinner->opts.dgt.min+spinner->cur_pos, buf);
            set_min_len(buf, spinner->opts.dgt.min_len);
            return;
        case SPINNER_TYPE_STR_RAM:
            strncpy(buf, spinner->opts.str.type.opts_ram[spinner->cur_pos], 
                    SCREEN_WIDTH_CHARS-2);
            return;
        case SPINNER_TYPE_STR_PGM:
            strncpy_P(buf, get_str_arr(spinner->opts.str.type.opts_pgm, 
                                       spinner->cur_pos), 
                      SCREEN_WIDTH_CHARS-2);
            return;
    }
}

void g_spinner_redraw(struct g_spinner *spinner) {
    char buf[SCREEN_WIDTH_CHARS+2];
    char buf_len;
    char x1=spinner->start_pos.x;
    char y1=spinner->start_pos.y;
    char x2;
    char x_max=spinner->start_pos.x+CHAR_WIDTH*(spinner->max_chars+2);
    char y2=spinner->start_pos.y+LINE_HEIGHT;
    memset(buf, 0, SCREEN_WIDTH_CHARS+2);
    buf[0]='<';
    get_str(buf+1, spinner);
    buf_len=strlen(buf);
    buf[buf_len++]='>';
    buf[buf_len]=0;
    debug_print("spinner: buf='%s' len=%d max_len=%d\n", buf, buf_len, spinner->max_chars);
    x2=spinner->start_pos.x+CHAR_WIDTH*(buf_len);
    clear_region(x1, y1, x_max, y2);
    draw_text(x1, y1, buf, 0, 0);
    if(spinner->is_selected)
        invert_region(x1, y1, x2, y2);
    flush_screen();
}

void g_spinner_setup_dgt(char start_x, char start_y, short min, short max,
                         char min_len, short pos, struct g_spinner *spinner)
{
    spinner->type = SPINNER_TYPE_DGT;
    spinner->is_selected = 0;
    spinner->start_pos.x = start_x;
    spinner->start_pos.y = start_y;
    spinner->opts.dgt.min = min;
    spinner->opts.dgt.max = max;
    spinner->cur_pos = pos;
    spinner->max_chars = 4; /*-32767...32767*/
    {
        char min_dig = short_str_len(min);
        char max_dig = short_str_len(max);
        spinner->max_chars=min_dig > max_dig ? min_dig : max_dig;
    }
    spinner->opts.dgt.min_len=min_len;
    g_spinner_redraw(spinner);
}

void g_spinner_setup_str_mem(char start_x, char start_y, char *opts[], 
                             char opts_max, short pos, struct g_spinner *spinner)
{
    spinner->type = SPINNER_TYPE_STR_RAM;
    spinner->is_selected = 0;
    spinner->start_pos.x = start_x;
    spinner->start_pos.y = start_y;
    spinner->opts.str.type.opts_ram = opts;
    spinner->opts.str.opts_max = opts_max;
    spinner->cur_pos = pos;
    spinner->max_chars = get_max_arr_len(opts, opts_max);
   
    g_spinner_redraw(spinner);
}

void g_spinner_setup_str_pgm(char start_x, char start_y, PGM_P const opts[], 
                             char opts_max, short pos, struct g_spinner *spinner)
{
    spinner->type = SPINNER_TYPE_STR_PGM;
    spinner->is_selected = 0;
    spinner->start_pos.x = start_x;
    spinner->start_pos.y = start_y;
    spinner->opts.str.type.opts_pgm = opts;
    spinner->opts.str.opts_max = opts_max;
    spinner->cur_pos = pos;
    spinner->max_chars = get_max_arr_len_P(opts, opts_max);

    g_spinner_redraw(spinner);
}

char g_spinner_set_pos(short pos, struct g_spinner *spinner)
{
    short max, min;
    switch(spinner->type) {
        case SPINNER_TYPE_STR_RAM:
        case SPINNER_TYPE_STR_PGM:
            max = spinner->opts.str.opts_max-1;
            min = 0;
            break;
        case SPINNER_TYPE_DGT:
            max = spinner->opts.dgt.max;
            min = spinner->opts.dgt.min;
            break;
        default:
            return -1;
    }
    if(pos+min > max || pos < 0) {
        debug_print("spinner: pos %d is out of range (%d,%d)\n", pos, min,max);
        return -1;
    }
    spinner->cur_pos = pos;
    g_spinner_redraw(spinner);
    return 0;
}

void g_spinner_set_selected(struct g_spinner *spinner, char is_selected)
{
    if(spinner->is_selected != is_selected) {
        spinner->is_selected = is_selected;
        g_spinner_redraw(spinner);
    }
}

char g_spinner_up(struct g_spinner *spinner)
{
    return g_spinner_set_pos(spinner->cur_pos+1, spinner);
}

char g_spinner_down(struct g_spinner *spinner)
{
    return g_spinner_set_pos(spinner->cur_pos-1, spinner);
}

short g_spinner_get_cur_pos(struct g_spinner *spinner)
{
    return spinner->cur_pos;
}
