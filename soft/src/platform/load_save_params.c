#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <console/console.h>
#include "load_save_params.h"


const static PROGMEM struct params_load params_load_fallback = {
    .pilot=9911,
    .sync1=3049,
    .sync2=3360,
    .data1=7817,
    .data0=3909,
    .duration_header=5,
    .duration_data=2,
    .pause_block=2000
};


const static PROGMEM struct params_save params_save_fallback = {
    .pilot_max=13714,
    .pilot_min=6857,
    .sync1_max=5028,
    .sync_total_max=16430,
    .zero_threshold=10057,
    .data_total_max=23058,
    .min_pilot_count=256
};

static EEMEM struct params_load params_load_eeprom;
static EEMEM struct params_save params_save_eeprom;

const static PROGMEM char STR_PARAMS_LOAD_FALLBACK[]="Loading fallback params_load...\n";
const static PROGMEM char STR_PARAMS_LOAD_LOAD[]="Loading params_load...\n";
const static PROGMEM char STR_PARAMS_SAVE_FALLBACK[]="Loading fallback params_save...\n";
const static PROGMEM char STR_PARAMS_SAVE_LOAD[]="Loading params_save...\n";
const static PROGMEM char STR_PARAMS_LOAD_SAVE[]="Saving params_load:\n";
const static PROGMEM char STR_PARAMS_SAVE_SAVE[]="Saving params_save:\n";

const static PROGMEM char STR_PARAMS_LOAD_PILOT[]="\tpilot:   ";
const static PROGMEM char STR_PARAMS_LOAD_SYNC1[]="\tsync1:   ";
const static PROGMEM char STR_PARAMS_LOAD_SYNC2[]="\tsync2:   ";
const static PROGMEM char STR_PARAMS_LOAD_DATA1[]="\tdata1:   ";
const static PROGMEM char STR_PARAMS_LOAD_DATA0[]="\tdata0:   ";
const static PROGMEM char STR_PARAMS_LOAD_DURHD[]="\tdurHead: ";
const static PROGMEM char STR_PARAMS_LOAD_DURDT[]="\tdurData: ";
const static PROGMEM char STR_PARAMS_LOAD_PAUSE[]="\tpause:   ";

const static PROGMEM char STR_PARAMS_SAVE_PILOT_MAX[]      ="\tpilot_max:       ";
const static PROGMEM char STR_PARAMS_SAVE_PILOT_MIN[]      ="\tpilot_min:       ";
const static PROGMEM char STR_PARAMS_SAVE_SYNC1_MAX[]      ="\tsync1_max:       ";
const static PROGMEM char STR_PARAMS_SAVE_SYNC_TOTAL_MAX[] ="\tsync_total_max:  ";
const static PROGMEM char STR_PARAMS_SAVE_ZERO_THRESHOLD[] ="\tzero_threshold:  ";
const static PROGMEM char STR_PARAMS_SAVE_DATA_TOTAL_MAX[] ="\tdata_total_max:  ";
const static PROGMEM char STR_PARAMS_SAVE_MIN_PILOT_COUNT[]="\tmin_pilot_count: ";

static void print_params_load(struct params_load *p)
{
    console_put_msg_short(STR_PARAMS_LOAD_PILOT, p->pilot);
    console_put_msg_short(STR_PARAMS_LOAD_SYNC1, p->sync1);
    console_put_msg_short(STR_PARAMS_LOAD_SYNC2, p->sync2);
    console_put_msg_short(STR_PARAMS_LOAD_DATA1, p->data1);
    console_put_msg_short(STR_PARAMS_LOAD_DATA0, p->data0);
    console_put_msg(STR_PARAMS_LOAD_DURHD, p->duration_header);
    console_put_msg(STR_PARAMS_LOAD_DURDT, p->duration_data);
    console_put_msg_short(STR_PARAMS_LOAD_PAUSE, p->pause_block);
}

static void print_params_save(struct params_save *p)
{
    console_put_msg_short(STR_PARAMS_SAVE_PILOT_MAX, p->pilot_max);
    console_put_msg_short(STR_PARAMS_SAVE_PILOT_MIN, p->pilot_min);
    console_put_msg_short(STR_PARAMS_SAVE_SYNC1_MAX, p->sync1_max);
    console_put_msg_short(STR_PARAMS_SAVE_SYNC_TOTAL_MAX, p->sync_total_max);
    console_put_msg_short(STR_PARAMS_SAVE_ZERO_THRESHOLD, p->zero_threshold);
    console_put_msg_short(STR_PARAMS_SAVE_DATA_TOTAL_MAX, p->data_total_max);
    console_put_msg_short(STR_PARAMS_SAVE_MIN_PILOT_COUNT, p->min_pilot_count);
}

char get_params_load(struct params_load *p)
{
    eeprom_read_block(p, &params_load_eeprom, sizeof(params_load_eeprom));
    console_puts_P(STR_PARAMS_LOAD_LOAD);
    print_params_load(p);
    if(p->pilot==0xffff) {
        memcpy_P(p, &params_load_fallback, sizeof(params_load_fallback));
        console_puts_P(STR_PARAMS_LOAD_FALLBACK);
        print_params_load(p);
        eeprom_write_block(p, &params_load_eeprom, sizeof(params_load_eeprom));
    }
    return 0;
}

char get_params_save(struct params_save *p)
{
    eeprom_read_block(p, &params_save_eeprom, sizeof(params_save_eeprom));
    console_puts_P(STR_PARAMS_SAVE_LOAD);
    print_params_save(p);
    if(p->pilot_max==0xffff) {
        memcpy_P(p, &params_save_fallback, sizeof(params_save_fallback));
        console_puts_P(STR_PARAMS_SAVE_FALLBACK);
        print_params_save(p);
        eeprom_write_block(p, &params_save_eeprom, sizeof(params_save_eeprom));
    }
    return 0;
}

char save_params_load(struct params_load *p)
{
    console_puts_P(STR_PARAMS_LOAD_SAVE);
    print_params_load(p);
    eeprom_write_block(p, &params_load_eeprom, sizeof(params_load_eeprom));
    return 0;
}

char save_params_save(struct params_save *p)
{
    console_puts_P(STR_PARAMS_SAVE_SAVE);
    print_params_save(p);

    eeprom_write_block(p, &params_save_eeprom, sizeof(params_save_eeprom));
    return 0;
}
