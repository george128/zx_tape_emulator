#include <console/console.h>
#include <keyboard/keyboard.h>
#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <graphics/graphics.h>
#include <platform/timer_in.h>
#include <disk/ff.h>
#include <disk/diskio.h>
#include <utils/strconv.h>
#include <string.h>
#include "module_main.h"
#include "screen_save.h"

#define MAX_BLOCKS 15

struct data {
    char path[PATH_MAX];
    FATFS fatfs;
    FIL fil;
    struct timer_in_state timer_in_state;
    char block_no;
    unsigned short block_len;
    char sum_test;
    FSIZE_t file_pos;
};


#define shm ((struct data *)get_shared_mem())

PROGMEM static const char STR_SCREEN_SAVE_LISTENING[]  = "Listening...";
PROGMEM static const char STR_TMP_FILE[]  = "/save.tap";
PROGMEM static const char STR_SCREEN_SAVE_FILE_ERR[]  = "File err: ";
PROGMEM static const char STR_SCREEN_SAVE_FILE_CLOSE[]  = "File close: ";
PROGMEM static const char STR_SCREEN_SAVE_WRITE_ERR[]  = "Write err: ";
PROGMEM static const char STR_SCREEN_SAVE_SEEK_ERR[]  = "Seek err: ";
PROGMEM static const char STR_SCREEN_SAVE_UMOUNT[]  = "Umount: ";
PROGMEM static const char STR_SCREEN_SAVE_BLOCK[]  = "block len=";
PROGMEM static const char STR_SCREEN_SAVE_FILE_RES[]  = "File res: ";
PROGMEM static const char STR_SCREEN_SAVE_WB[]  = "WB: ";
PROGMEM static const char STR_SCREEN_SAVE_SEEK[]  = "Seek: ";
PROGMEM static const char STR_SCREEN_SAVE_BLOCK_LEN[]  = "BlockLen:";
PROGMEM static const char STR_SCREEN_SAVE_SUM_ERR[]  = "Checksum error: 0x";

static char screen_save_openfile() {
    char code=0;

    memset(&shm->fatfs, 0, sizeof(shm->fatfs));
    memset(&shm->fil, 0, sizeof(shm->fil));

    shm->fatfs.drv=0; /*drive 0*/
    FRESULT res=f_mount(&shm->fatfs,"0:",1);
    if(res!=FR_OK) {
        code = 10;
        console_put_msg(STR_SCREEN_SAVE_FILE_RES, res);
        goto err;
    }
    res=f_open(&shm->fil,shm->path, FA_WRITE|FA_CREATE_ALWAYS);
    if(res!=FR_OK) {
        code = 20;
        console_put_msg(STR_SCREEN_SAVE_FILE_RES, res);
    }
 err:
    console_puts(shm->path);
    console_next_line();
    return code;

}

static void screen_save_write_err(FRESULT res)
{
    char buf[6];
    console_put_msg(STR_SCREEN_SAVE_WRITE_ERR, res);
    clear_screen();
    draw_text(size_x(0), line_y(3), STR_SCREEN_SAVE_WRITE_ERR, 0, 1);
    byte2dec(res, buf);
    draw_text(size_x(12), line_y(3), buf, 0, 1);
    flush_screen();
    timer_in_done();
}

static short screen_save_next_data(char *buf, unsigned short len, char is_first, char is_last)
{
    UINT wb;
    char buf_block_len[2]={0,0};
    unsigned short i;
    FRESULT res;
    char is_end_block=0;

    console_puts_P(STR_SCREEN_SAVE_BLOCK);
    console_put_short(len);
    console_putc(' ');
    console_putd(is_first);
    console_putc(' ');
    console_putd(is_last);
    console_next_line();
    if(is_first) {
        shm->block_no++;
        shm->block_len=len;
        shm->sum_test=0;
        shm->file_pos=f_tell(&shm->fil);
        if(is_last) {
            memcpy(buf_block_len, &len, sizeof(len));
        }
        res=f_write(&shm->fil,buf_block_len,sizeof(len),&wb);
        if(res!=FR_OK) {
            screen_save_write_err(res);
            return -1;
        }
        if((unsigned char)*buf==0xff)
            is_end_block=1;
    } else {
        shm->block_len+=len;
        is_end_block=1;
    }
    for(i=0;i<len;i++) {
        shm->sum_test^=buf[i];
    }
    res=f_write(&shm->fil,buf,len,&wb);
    if(res!=FR_OK) {
        screen_save_write_err(res);
        return -1;
    }
    console_puts_P(STR_SCREEN_SAVE_WB);
    console_put_short(wb);
    console_next_line();
    if(is_last) {
        if(!is_first) {
            console_puts_P(STR_SCREEN_SAVE_SEEK);
            console_put_short(shm->file_pos);
            console_next_line();
            res=f_lseek(&shm->fil, shm->file_pos);
            if(res==FR_OK) {
                memcpy(buf_block_len, &shm->block_len, sizeof(shm->block_len));
                res=f_write(&shm->fil,buf_block_len,2,&wb);
            } else {
                console_put_msg(STR_SCREEN_SAVE_SEEK_ERR, res);
            }
        }
        if(is_end_block && shm->block_no < SCREEN_MAX_LINES) {
            char buf_short[6];
            draw_text(size_x(0), line_y(shm->block_no), STR_SCREEN_SAVE_BLOCK_LEN, 0, 1);
            draw_text(size_x(10), line_y(shm->block_no), short10_2_str(shm->block_len,buf_short), 0, 0);
            if(shm->sum_test != 0) {
                console_put_msg(STR_SCREEN_SAVE_SUM_ERR, shm->sum_test);
                invert_region(size_x(0), line_y(shm->block_no), SCREEN_WIDTH-1, line_y(shm->block_no+1));
            }
            flush_screen();
        }
    }
    return wb;
}

static void screen_save_found_header(struct tap_header *h)
{
     char buf[SCREEN_WIDTH_CHARS], *ptr=buf;
    char i=0,j=0;

    if(shm->block_no==SCREEN_MAX_LINES)
        return;

    switch(h->type) {
        case 0: /*Programm*/
            *ptr='P';
            break;
        case 1: /*Numbers array*/
            *ptr='N';
            break;
        case 2: /*Charracters array*/
            *ptr='A';
            break;
        case 3: /*Code*/
            *ptr='C';
            break;
        default: /*Undefined*/
            *ptr='U';
    }
    ptr++;
    *(ptr++)=':';
    while((h->name[i]<0x20 || h->name[i]>0x7f) && i<10)i++;
    while(i<10 && j<6) {
        if(h->name[i]>=0x20 && h->name[i]<=0x7f) {
            *(ptr++)=h->name[i];
            j++;
        }
        i++;
    }
    
    *(ptr++)=':';
    {
        char buf_short[6];
        strncpy(ptr,short10_2_str(h->data_len,buf_short), 6);
    }
    draw_text(size_x(0), line_y(shm->block_no), buf, 0, 0);
    flush_screen();
}

PROGMEM const static struct timer_in_handler timer_in_handler= {
    .save_next_data = screen_save_next_data,
    .found_header = screen_save_found_header,
};

static void screen_save_display() 
{
    char code;
    clear_screen();
    strcpy_P(shm->path,STR_TMP_FILE);
    if((code=screen_save_openfile())!=0) {
        char buf[4];
        console_put_msg(STR_SCREEN_SAVE_FILE_ERR, code);
        draw_text(size_x(0),line_y(3),STR_SCREEN_SAVE_FILE_ERR,0,1);
        byte2dec(code, buf);
        draw_text(size_x(11),line_y(3),buf,0,0);
        flush_screen();
        return;
    }
    shm->block_no=0;
    timer_in_init(&shm->timer_in_state);
    timer_in_start(&timer_in_handler);
    draw_text(size_x(0),line_y(0),STR_SCREEN_SAVE_LISTENING,0,1);
    flush_screen();
}

static void screen_save_destroy()
{
    timer_in_done();
    FRESULT res=f_close(&shm->fil);
    console_put_msg(STR_SCREEN_SAVE_FILE_CLOSE, res);
    res=f_mount(NULL,"0:",1); // umount
    console_put_msg(STR_SCREEN_SAVE_UMOUNT, res);
    {
        char c=0;
        disk_ioctl(0,CTRL_POWER, &c);
    } 
}

static void screen_save_key_press(char keycode)
{
    switch(keycode) {
        case KEY_LEFT:
            module_process_event(PUBLIC_EVENT_RETURN);
            break;
    }
}
PROGMEM const struct screen screen_save=
{
    .display=screen_save_display,
    .destroy=screen_save_destroy,
    .key_press=screen_save_key_press,
    .shm_size=sizeof(struct data)
};
