#ifndef _PROGRESS_BAR_H_
#define _PROGRESS_BAR_H_

struct progress_bar {
    char percentage;
    char *text;
    char is_progmem;
    char x;
    char y;
};

void set_progress_bar(struct progress_bar *state);

#endif /*_PROGRESS_BAR_H_*/
