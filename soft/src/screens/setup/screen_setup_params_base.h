#ifndef _SCREEN_SETUP_PARAMS_BASE_H_
#define _SCREEN_SETUP_PARAMS_BASE_H_

#include <widgets/list.h>
#include <widgets/keyb.h>

struct param_rec {
    const char *text;
    const char *postfix;
    int offset;
    void (*to_str)(void *val, char *buf, void (*conv_to)(void *val));
    void (*from_str)(char *buf, void *val, void (*conv_from)(void *val));
    void (*conv_to)(void *val);
    void (*conv_from)(void *val);
};

struct params_data {
    const struct param_rec *param_recs;
    void (*save_params)(void *params);
    void (*get_params)(void *params);
    char num_recs;
    void *params;
    struct g_list list;
    struct g_keyb keyb;
    char keyb_buf[10];
    char offset;
    char is_edit;
};

extern const char STR_POSTFIX_US[];
extern const char STR_POSTFIX_MS[];
extern const char STR_POSTFIX_S[];

void params_char_to_str(void *val, char *buf,void (*conv_to)(void *val));
void params_char_from_str(char *buf, void *val, void (*conv_from)(void *val));
void params_short_to_str(void *val, char *buf, void (*conv_to)(void *val));
void params_short_from_str(char *buf, void *val, void (*conv_from)(void *val));
void params_conv_to_us(void *val);
void params_conv_from_us(void *val);

void screen_setup_params_init(struct params_data *data, const struct param_rec *recs, char num_recs);
void screen_setup_params_key_press(struct params_data *data, char keycode);



#endif /*_SCREEN_SETUP_PARAMS_BASE_H_*/
