#ifndef _WIDGETS_LINE_EDIT_H_
#define _WIDGETS_LINE_EDIT_H_

#include <graphics/graphics.h>

struct g_line_edit
{
    char buf[SCREEN_WIDTH_CHARS+1];
    char cur_pos;
    char max_pos;
    char is_start;
    struct pos start_pos;
};

void g_line_edit_init(char *str_init, char max_len, char x_start, char y_start,
                      struct g_line_edit *line_edit);
void g_line_edit_process_key(char keycode, struct g_line_edit *line_edit);
char *g_line_edit_get_str(struct g_line_edit *line_edit);
void g_line_edit_done(struct g_line_edit *line_edit);

#endif /*_WIDGETS_LINE_EDIT_H_*/
