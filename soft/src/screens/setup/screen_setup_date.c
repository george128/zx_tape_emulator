#include <string.h>
#include <utils/strconv.h>
#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <graphics/graphics.h>
#include <timer/timer.h>
#include <rtc/rtc.h>
#include <keyboard/keyboard.h>
#include <console/console.h>
#include <widgets/spinner.h>
#include "module_setup.h"
#include "screen_setup_date.h"


PROGMEM static const char STR_SCREEN_SETUP_YEAR_2000[]="20";

struct date_field_opt {
    char x_pos;
    char y_pos;
    short min;
    short max;
};

PROGMEM static const struct date_field_opt DATE_POS[]={
    /*day*/       {size_x(1), line_y(1), 1, 31},
    /*month*/     {size_x(4), line_y(1), 1, 12},
    /*year*/      {size_x(7), line_y(1), 2000, 2099},
    /*hour*/      {size_x(1), line_y(2), 0, 59},
    /*minute*/    {size_x(4), line_y(2), 0, 59},
    /*seconds*/   {size_x(7), line_y(2), 0, 59},
};

struct data {
    struct timer timer_time;
    unsigned char timer_time_id;
    char date_edit_cnt;
    struct rtc rtc;
    struct g_spinner spinner_date;
    char cur_timer_id;
    char timer_edit_cnt;
};

#define shm ((struct data *) (get_shared_mem()))

#ifdef _SCREEN_DATE_DEBUG
PROGMEM static const char STR_SCREEN_SETUP_F_NO[]="setup_date: field=";
PROGMEM static const char STR_SCREEN_SETUP_F_OPTS[]="setup_date: opts: ";
PROGMEM static const char STR_SCREEN_SETUP_SPIN[]="setup_date: spin ";
#endif

static void construct_date_field(char *buf, char field_no, 
                                 char val10, char val,
                                 char *prefix, char delim)
{
    struct date_field_opt f;
    memcpy_P(&f, DATE_POS + field_no, sizeof(f));
#ifdef _SCREEN_DATE_DEBUG
    console_put_msg(STR_SCREEN_SETUP_F_NO, field_no);
    console_puts_P(STR_SCREEN_SETUP_F_OPTS);
    console_putd(f.x_pos);
    console_putc(' ');
    console_putd(f.y_pos);
    console_putc(' ');
    console_put_short(f.min);
    console_putc(' ');
    console_put_short(f.max);
    console_next_line();
#endif
    if(shm->date_edit_cnt == field_no) {
        short dgt = val+val10*10;
        char pos = dgt>=f.min?dgt-f.min:dgt;
        
#ifdef _SCREEN_DATE_DEBUG
        console_puts_P(STR_SCREEN_SETUP_SPIN);
        console_put_short(dgt);
        console_putc(' ');
        console_putd10(pos);
        console_next_line();
#endif
        debug_print("date field=%d f.min=%d f.max=%d\n",field_no, f.min,f.max);
        g_spinner_setup_dgt(f.x_pos-size_x(1), f.y_pos, f.min, f.max, 2,
                            pos, &shm->spinner_date);
    } else {
        char *ptr=buf;

        if(prefix != NULL) {
            strcpy(ptr, prefix);
            ptr+=strlen(prefix);
        }
        *ptr++=byte2char(val10);
        *ptr++=byte2char(val);
        if(delim > 0)
            *ptr++=delim;
    }
}

static void show_date(struct rtc *rtc)
{
    char buf_delim[4], buf_line[25];
    strncpy_P(buf_delim, STR_SCREEN_SETUP_YEAR_2000, 4);

    construct_date_field(buf_line, 0, rtc->day10, rtc->day, NULL, '/');
    construct_date_field(buf_line+3, 1, rtc->month10, rtc->month, NULL, '/');
    construct_date_field(buf_line+6, 2, rtc->year10, rtc->year, buf_delim, '\n');
    buf_line[11]=' ';
    construct_date_field(buf_line+12, 3, rtc->hour10, rtc->hour, NULL, ':');
    construct_date_field(buf_line+15, 4, rtc->min10, rtc->min, NULL, ':');
    construct_date_field(buf_line+18, 5, rtc->sec10, rtc->sec, NULL, 0);
    buf_line[20]=' ';
    buf_line[21]=0;
    clear_region(size_x(0), line_y(1), SCREEN_WIDTH, line_y(0)+LINE_HEIGHT*2-1);
    draw_text(size_x(1),line_y(1),buf_line,1,0);
    
    if(shm->date_edit_cnt!=-1)
        g_spinner_redraw(&shm->spinner_date);
    else
        flush_screen();
}

static void screen_setup_date_callback(unsigned char timer_id, const void *data)
{
    if(shm->date_edit_cnt<0) {
        rtc_get(&shm->rtc);
        show_date(&shm->rtc);
    }
}

static void screen_setup_date_display() 
{
    clear_screen();
    shm->date_edit_cnt=-1;
    shm->timer_time.period=1000;
    shm->timer_time.callback=screen_setup_date_callback;
    shm->timer_time_id=timer_setup(&shm->timer_time);

    rtc_get(&shm->rtc);
    show_date(&shm->rtc);
    flush_screen();
}

static void screen_setup_date_destroy()
{
    timer_free(shm->timer_time_id);
}

#ifdef _SCREEN_DATE_DEBUG
PROGMEM static const char STR_SCREEN_SETUP_SPIN2BCD[]="setup_date: spin2bcd ";
#define spinner2bcd_dump(min_val, val, dec, dec10) \
    console_puts_P(STR_SCREEN_SETUP_SPIN2BCD);\
    console_put_short(min_val);\
    console_putc(' ');\
    console_put_short(val);\
    console_putc(' ');\
    console_putd10(dec);\
    console_putc(' ');\
    console_putd10(dec10);\
    console_next_line();
#else
#define spinner2bcd_dump(min_val, val, dec, dec10)
#endif

#define spinner2bcd(min_val, dec, dec10) {\
    short val = min_val+g_spinner_get_cur_pos(&shm->spinner_date);\
    dec=val%10;\
    dec10=(val/10)%10;\
    spinner2bcd_dump(min_val, val, dec, dec10);\
    debug_print("min=%d val=%d dec=%d dec10=%d\n", min_val, val, dec, dec10);\
    }

static void save_date() {

    switch(shm->date_edit_cnt) {
        case 0:
            spinner2bcd(1, shm->rtc.day, shm->rtc.day10);
            break;
        case 1:
            spinner2bcd(1, shm->rtc.month, shm->rtc.month10);
            break;
        case 2:
            spinner2bcd(2000, shm->rtc.year, shm->rtc.year10);
            break;
        case 3:
            spinner2bcd(0, shm->rtc.hour, shm->rtc.hour10);
            break;
        case 4:
            spinner2bcd(0, shm->rtc.min, shm->rtc.min10);
            break;
        case 5:
            spinner2bcd(0, shm->rtc.sec, shm->rtc.sec10);
#ifdef _SCREEN_DATE_DEBUG
            console_putmem(&shm->rtc, sizeof(shm->rtc));
            console_next_line();
#endif
            rtc_set(&shm->rtc);
            break;
    }
}

static void update_date_edit() {
    switch(shm->date_edit_cnt) {
        case -1:
            shm->date_edit_cnt=0;
            break;
        case 5:
            save_date();
            shm->date_edit_cnt=-1;
            break;
        default:
            save_date();
            shm->date_edit_cnt++;
    }
    show_date(&shm->rtc);
}

static void reset_date()
{
    shm->date_edit_cnt=-1;
    rtc_get(&shm->rtc);
    show_date(&shm->rtc);
}

static inline char is_edit_date()
{
    return shm->date_edit_cnt>=0;
}

static void screen_setup_date_key_press(char keycode)
{
    switch(keycode) {
        case KEY_UP:
            if(is_edit_date())
                g_spinner_up(&shm->spinner_date);
            break;
        case KEY_DOWN:
            if(is_edit_date())
                g_spinner_down(&shm->spinner_date);
            break;
        case KEY_RIGHT:
            update_date_edit();
            break;
        case KEY_LEFT:
            if(is_edit_date())
                reset_date();
            else 
                module_process_event(PRIVATE_EVENT_SCREEN_SETUP_MAIN);
            break;
        }
}

PROGMEM const struct screen screen_setup_date=
{
    .display=screen_setup_date_display,
    .destroy=screen_setup_date_destroy,
    .key_press=screen_setup_date_key_press,
    .shm_size=sizeof(struct data)
};
