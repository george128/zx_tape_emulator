#include "rtc.h"
#include "pcf8583.h"

void rtc_init()
{
    pcf8583_init();
}

void rtc_test()
{
    pcf8583_test();
}


void rtc_get(struct rtc *rtc)
{
    pcf8583_get_date(rtc);
}

void rtc_set(struct rtc *rtc)
{
    pcf8583_set_date(rtc);
}
