#ifndef _SCR_MODULE_H_
#define _SCR_MODULE_H_

#include <platform/utils.h>

#define PUBLIC_EVENT_START 0
#define PUBLIC_EVENT_DONE 1
#define PUBLIC_EVENT_RETURN 2

#define STATE_INIT 0

#define MODULE_DEBUG

struct screen;

struct module
{
    void (*init)();
    void (*process_event)(char event_id);
    const struct screen *(*search_screen)(char screen_id);
    void (*set_data)(char data_id, void *ptr);
    void *(*get_data)(char data_id);
    const char *name;
};

void module_init();

void module_set_screen(char screen_id);
void module_process_event(char event_id);
void module_key_press(char keycode);
void module_pop();
void module_push(const struct module *module);

void module_set_data(char data_id, void *ptr);
void *module_get_data(char data_id);

void module_task();

void module_process_event_common(const struct module *module, char event_id);
void module_unknown_screen(const struct module *module, char screen_id);
const struct screen *module_search_screen(char screen_id, char max_screen,
                                          const struct module *module,
                                          PGM_P const *screens_arr);

#ifdef MODULE_DEBUG
void module_debug_print_screen(const struct module *module, const struct screen *scr, char screen_id);
#else
#define module_debug_print_screen(module, scr, screen_id)
#endif /*MODULE_DEBUG*/

#endif /*_SCR_MODULE_H_*/
