#ifndef _TIMER_IN_H_
#define _TIMER_IN_H_

#include <zx_files/tap.h>
#include <timer/timer.h>
#include <platform/load_save_params.h>

#define TAPE_IN_BUF_SIZE 256

struct timer_in_handler {
    void (*found_header)(struct tap_header *h);
    short (*save_next_data)(char *buf, unsigned short len, char is_first, char is_last);
};

struct inbuf {
    char buf[TAPE_IN_BUF_SIZE];
    unsigned short len;
    char is_first, is_last;
    char flags;
};

struct timer_in_state {
    char timer_ovf; 
    unsigned short pilot_cnt;
    struct inbuf inbuf[2];
    volatile char state, bit_pos,buf_no, last_buf_no;
    unsigned short buf_pos;
    struct timer t;
    unsigned char timer_id;
    unsigned short cnt_prev, period_prev;
    volatile char last_state;
    struct params_save params_save;
    char sound_on;
};

void timer_in_init(struct timer_in_state *state_init);
void timer_in_start(const struct timer_in_handler *handler);
void timer_in_done();

#endif /*_TIMER_OUT_H_*/
