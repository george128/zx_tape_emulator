#include "screen_setup_main.h"

#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <screens/setup/module_setup.h>
#include <graphics/graphics.h>
#include <keyboard/keyboard.h>
#include <widgets/list.h>
#include <platform/utils.h>

PROGMEM static const char STR_SCREEN_SETUP_MAIN_DATE_DIME[]="Set Date/Time";
PROGMEM static const char STR_SCREEN_SETUP_MAIN_SETTINGS[]="Settings";
PROGMEM static const char STR_SCREEN_SETUP_MAIN_LOAD[]="Load timings";
PROGMEM static const char STR_SCREEN_SETUP_MAIN_SAVE[]="Save timinigs";
PROGMEM static const char STR_SCREEN_SETUP_MAIN_CLEAR[]="Restor timings";
PROGMEM static const char STR_SCREEN_SETUP_MAIN_LCD_CONTRAST[]="LCD contrast";

PROGMEM static const char SCREEN_SETUP_MAIN_MENU_EVENTS[] = {
    PRIVATE_EVENT_SCREEN_SETUP_DATE,
    PRIVATE_EVENT_SCREEN_SETUP_SETTINGS,
    PRIVATE_EVENT_SCREEN_SETUP_LOAD,
    PRIVATE_EVENT_SCREEN_SETUP_SAVE,
    PRIVATE_EVENT_SCREEN_SETUP_CLEAR,
    PRIVATE_EVENT_SCREEN_SETUP_LCD_CONTRAST,
};

struct data {
    struct g_list list;
};

#define shm ((struct data *) (get_shared_mem()))


static void screen_setup_main_display() 
{
    struct g_list *list = &shm->list;
    unsigned char x_start = size_x(0);
    unsigned char list_width = SCREEN_WIDTH - x_start - 1;

    clear_screen();
    draw_text(x_start,line_y(0),STR_SCREEN_SETUP_MAIN_DATE_DIME,0,1);
    draw_text(x_start,line_y(1),STR_SCREEN_SETUP_MAIN_SETTINGS,0,1);
    draw_text(x_start,line_y(2),STR_SCREEN_SETUP_MAIN_LOAD,0,1);
    draw_text(x_start,line_y(3),STR_SCREEN_SETUP_MAIN_SAVE,0,1);
    draw_text(x_start,line_y(4),STR_SCREEN_SETUP_MAIN_CLEAR,0,1);
    draw_text(x_start,line_y(5),STR_SCREEN_SETUP_MAIN_LCD_CONTRAST,0,1);

    list->num_elements=6;
    list->is_vert=1;
    list->width=list_width;
    list->x_start=x_start;
    list->y_start=line_y(0);
    g_list_init(list, 0);
}

static void screen_setup_main_send_event(char list_pos)
{
    char event_id=pgm_read_byte(&SCREEN_SETUP_MAIN_MENU_EVENTS[list_pos]);
    module_process_event(event_id);
}

static void screen_setup_main_key_press(char keycode)
{
    switch(keycode) {
        case KEY_UP:
            g_list_up(&shm->list);
            break;
        case KEY_DOWN:
            g_list_down(&shm->list);
            break;
        case KEY_LEFT:
            module_process_event(PUBLIC_EVENT_RETURN);
            break;
        case KEY_ENTER:
            screen_setup_main_send_event(g_list_get_current(&shm->list));
            break;
    }


}

PROGMEM const struct screen screen_setup_main=
{
    .display=screen_setup_main_display,
    .destroy=0,
    .key_press=screen_setup_main_key_press,
    .shm_size=sizeof(struct data)
};
