#include <string.h>
#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <graphics/graphics.h>
#include <keyboard/keyboard.h>
#include <widgets/list.h>
#include <widgets/keyb.h>
#include <disk/ff.h>
#include <disk/diskio.h>
#include <console/console.h>
#include <utils/strconv.h>
#include "module_main.h"
#include "screen_file_ops.h"

#define t ((struct timer *)get_shared_mem())

PROGMEM static const char STR_SCREEN_FILE_OPS_LOAD[]  = "Load into new";
PROGMEM static const char STR_SCREEN_FILE_OPS_RENAME[]= "Rename";
PROGMEM static const char STR_SCREEN_FILE_OPS_DELETE[]= "Delete";
PROGMEM static const char STR_SCREEN_FILE_OPS_YESNO[]= "Yes No";

PROGMEM static const char STR_UNLINK_RES[]="screen_file_ops: unlink res=";
PROGMEM static const char STR_RENAME_RES[]="screen_file_ops: rename res=";
PROGMEM static const char STR_FILE_OPS_ERR[]="Fileops err:";

struct data {
    char path[PATH_MAX];
    char edit_buf[PATH_MAX];
    struct g_list list;
    struct g_list list_ask;
    struct g_keyb keyb;
    char is_ask;
    char is_keyb;
};

#define shm ((struct data *) (get_shared_mem()))

static void screen_file_ops_create_ask_list()
{
    struct g_list *list = &shm->list_ask;
    draw_text(size_x(0),line_y(5),STR_SCREEN_FILE_OPS_YESNO,0,1);

    shm->is_ask=1;
    list->num_elements=2;
    list->is_vert=0;
    list->width=size_x(3);;
    list->x_start=0;
    list->y_start=line_y(5);
    g_list_init(list, 1);
}

static void screen_file_ops_create_keyb()
{
    
    char buf_len, *ptr;
    strncpy(shm->edit_buf, shm->path, PATH_MAX);
    ptr=strrchr(shm->edit_buf,'/');
    if(ptr==NULL) {
        ptr=shm->edit_buf;
    } else {
        ptr++;
    }
    buf_len=PATH_MAX-(ptr-shm->edit_buf);
    shm->is_keyb=1;

    g_keyb_init(&shm->keyb, ptr, buf_len, G_KEYB_TYPE_ALPHA);
}

static void screen_file_ops_redraw()
{
    clear_screen();
    draw_text(size_x(0),line_y(0),shm->path,0,0);
    draw_text(size_x(0),line_y(1),STR_SCREEN_FILE_OPS_LOAD,0,1);
    draw_text(size_x(0),line_y(2),STR_SCREEN_FILE_OPS_RENAME,0,1);
    draw_text(size_x(0),line_y(3),STR_SCREEN_FILE_OPS_DELETE,0,1);
    if(shm->list.num_elements>0) {
        g_list_redraw(&shm->list);
    }
}

static void screen_file_ops_display() 
{
    char *path=module_get_data(PRIVATE_DATA_EVENT_TAP_FILE);
    struct g_list *list = &shm->list;

    memmove(shm->path, path, PATH_MAX);
    list->num_elements=0;

    screen_file_ops_redraw();

    shm->is_ask=shm->is_keyb=0;
    list->num_elements=3;
    list->is_vert=1;
    list->width=SCREEN_WIDTH;
    list->x_start=0;
    list->y_start=line_y(1);
    g_list_init(list, 0);
}

static void screen_file_ops_destroy()
{
}

static void screen_file_ops_delete()
{
    FATFS fatfs;

    console_puts(shm->path);
    console_next_line();
    FRESULT res=f_mount(&fatfs,"0:",1);
    if(res != FR_OK) {
        char buf[3];
        draw_text(size_x(0),line_y(5),STR_FILE_OPS_ERR,0,1);
        draw_text(size_x(12), line_y(5), byte2str(res, buf),0,0);
        flush_screen();
    } else {
        res=f_unlink(shm->path);
        console_put_msg(STR_UNLINK_RES, res);
        if(res != FR_OK) {
            char buf[3];
            draw_text(size_x(0),line_y(5),STR_FILE_OPS_ERR,0,1);
            draw_text(size_x(12), line_y(5), byte2str(res, buf),0,0);
            flush_screen();
        }
    }
    res=f_mount(NULL,"0:",1); // umount
    {
        char c=0;
        disk_ioctl(0,CTRL_POWER, &c);
    } 
}

static void screen_file_ops_rename() {
    FATFS fatfs;

    console_puts(shm->path);
    console_next_line();
    console_puts(shm->edit_buf);
    console_next_line();

    FRESULT res=f_mount(&fatfs,"0:",1);
    if(res != FR_OK) {
        char buf[3];
        draw_text(size_x(0),line_y(5),STR_FILE_OPS_ERR,0,1);
        draw_text(size_x(12), line_y(5), byte2str(res, buf),0,0);
        flush_screen();
    } else {
        res=f_rename(shm->path, shm->edit_buf);
        console_put_msg(STR_RENAME_RES, res);
        if(res != FR_OK) {
            char buf[3];
            draw_text(size_x(0),line_y(5),STR_FILE_OPS_ERR,0,1);
            draw_text(size_x(12), line_y(5), byte2str(res, buf),0,0);
            flush_screen();
        }
    }
    res=f_mount(NULL,"0:",1); // umount
    {
        char c=0;
        disk_ioctl(0,CTRL_POWER, &c);
    } 
}

static void screen_file_ops_process_entry()
{
    switch(g_list_get_current(&shm->list)) {
        case 0: // Load into new
            module_process_event(PRIVATE_EVENT_SAVE_FILE);
            break;
        case 1: // Rename
            screen_file_ops_create_keyb();
            break;
        case 2: // Delete
            screen_file_ops_create_ask_list();
            break;
    }
}

static void screen_file_ops_key_press(char keycode)
{
    if(shm->is_keyb) {
        switch(g_keyb_process_key(&shm->keyb, keycode)) {
            case G_KEYB_EDIT:
                break;
            case G_KEYB_DONE:
                screen_file_ops_rename();
                module_process_event(PUBLIC_EVENT_RETURN);
            case G_KEYB_CANCEL:
                shm->is_keyb=0;
                screen_file_ops_redraw();
                break;
        }
    } else if(shm->is_ask) {
        switch(keycode) {
            case KEY_LEFT:
                g_list_up(&shm->list_ask);
                break;
            case KEY_DOWN:
                g_list_up(&shm->list_ask);
                break;
            case KEY_ENTER:
                clear_line(5);
                flush_screen();
                if(g_list_get_current(&shm->list_ask)==0) {
                    screen_file_ops_delete();
                    module_process_event(PUBLIC_EVENT_RETURN);
                }
                shm->is_ask=0;
                break;
        }
    } else {
        switch(keycode) {
            case KEY_UP:
                g_list_up(&shm->list);
                break;
            case KEY_DOWN:
                g_list_down(&shm->list);
                break;
            case KEY_LEFT:
                module_process_event(PUBLIC_EVENT_RETURN);
                break;
        case KEY_RIGHT:
            break;
        case KEY_ENTER:
            screen_file_ops_process_entry();
            break;
    }
    }
}

PROGMEM const struct screen screen_file_ops=
{
    .display=screen_file_ops_display,
    .destroy=screen_file_ops_destroy,
    .key_press=screen_file_ops_key_press,
    .shm_size=sizeof(struct data)
};
