#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include "tap.h"

static FILE *f;
static char *fp;
static char block_cnt;

static short get_next_data(char *buf, short len) 
{
    int rb;
    // long f_tell;
    if(f==NULL) {
        printf("Try to open file: '%s'\n", fp);
        f=fopen(fp, "r");
        if(f == NULL) {
            printf("Can't open file '%s': %s\n", fp, strerror(errno));
            exit(-1);
        }
    }
    // f_tell=ftell(f);
    rb = fread(buf, 1, len, f);
    if(rb == 0) {
        printf("EOF\n");
        fclose(f);
        f=NULL;
    }
    // printf("get_next_data: ftell=0x%lx len=%d rb=%d\n",f_tell, len, rb);
    return rb;
}

static void found_header(struct tap_header *h, unsigned long file_pos)
{
    char name[11];
    strncpy(name,h->name,10);
    name[10]=0;
    // printf("Header size=%lu\n",sizeof(*h));
    printf("Header: \n\tflag=%d\n\ttype=%d\n\tname=%10s\n\tdata_len=%d\n\tstart_line=%d\n\tprogram_len=%d\n\tchksum=%d\n\tfile_pos=0x%lx\n",
           h->flag, h->type, name, h->data_len, h->start_line, h->program_len, h->chksum, file_pos);
}

static void found_block(unsigned short len, unsigned long file_pos)
{
    printf("Block[%d] len: %u file_pos: 0x%lx\n",block_cnt++,len, file_pos);
}

static void log_msg(const char *format,...)
{
    va_list ap;
    char buf[1024];

    va_start(ap, format);
    vsnprintf(buf, 1024, format, ap);
    printf(buf);
    va_end(ap);
}

struct tap_handler tap = {
    .get_next_data=get_next_data,
    .found_header=found_header,
    .found_block=found_block,
    .log=log_msg,
};
int main(int argc, char *argv[])
{
    struct tap_state state;
    char ret;
    char sum=0;
    //char buf[16], *ptr;
    int i;
    
    if(argc<=1) {
        printf("USE: %s <file.tap>\n",argv[0]);
        return 10;
    }
    fp=argv[1];
    state.block_len=0;
    state.file_pos=0;
    while((ret=tap_process(&tap, &state))==TAP_STATE_PROCESS) {
	//printf("process len=%d\n",state.buf_len);
	for(i=0;i<state.buf_len;i++) {
	    sum^=state.buf[i];
	    /*sprintf(buf, "%02x",state.buf[i]);
	    if(strlen(buf)==2)
		ptr=buf;
	    else
		ptr=buf+6;
	    printf(" %s(%02x)", ptr, sum);
	    */
	}
	if(state.block_pos==state.block_len) {
	    printf(/*"\n"*/"end block len=%d file_pos=0x%lx sum=%u sum_test=%u\n", state.block_len,state.file_pos,state.buf[state.buf_len-1],sum);
	    state.block_len=0;
	    sum=0;
	} //else
	//    printf("\n");
    }
    printf("Done, ret=%d\n",ret);
    return 0;
}
