#include <string.h>
#include <platform/utils.h>
#include <console/console.h>
#include <utils/strings.h>
#include <platform/load_save_params.h>
#include "module_setup.h"
#include "screen_setup_main.h"
#include "screen_setup_date.h"
#include "screen_setup_settings.h"
#include "screen_setup_load.h"
#include "screen_setup_save.h"
#include "screen_setup_lcd_contrast.h"

PROGMEM static const char STR_SCR_MODULE_SETUP_NAME[]="setup";
PROGMEM static const char STR_SCR_MODULE_SET_DATA[]="setup: set data id=";
PROGMEM static const char STR_SCR_MODULE_GET_DATA[]="setup: get data id=";

static PGM_P const screens_arr[] PROGMEM = {
    (PGM_P)&screen_setup_main,
    (PGM_P)&screen_setup_date,
    (PGM_P)&screen_setup_settings,
    (PGM_P)&screen_setup_load,
    (PGM_P)&screen_setup_save,
    (PGM_P)&screen_setup_lcd_contrast,
};

static void module_setup_init()
{
    debug_puts("module_setup_init()\n");
}

static void module_setup_process_event(char event_id)
{
    switch(event_id) {
        case PUBLIC_EVENT_START:
        case PRIVATE_EVENT_SCREEN_SETUP_MAIN:
            module_set_screen(SCREEN_SETUP_MAIN);
            break;
        case PRIVATE_EVENT_SCREEN_SETUP_DATE:
            module_set_screen(SCREEN_SETUP_DATE);
            break;
        case PRIVATE_EVENT_SCREEN_SETUP_SETTINGS:
            module_set_screen(SCREEN_SETUP_SETTINGS);
            break;
        case PRIVATE_EVENT_SCREEN_SETUP_LOAD:
            module_set_screen(SCREEN_SETUP_LOAD);
            break;
        case PRIVATE_EVENT_SCREEN_SETUP_SAVE:
            module_set_screen(SCREEN_SETUP_SAVE);
            break;
        case PRIVATE_EVENT_SCREEN_SETUP_CLEAR:
        {
            struct params_load pl;
            struct params_save ps;
            memset(&pl,0xff,sizeof(pl));
            memset(&ps,0xff,sizeof(ps));
            save_params_load(&pl);
            save_params_save(&ps);
            module_set_screen(SCREEN_SETUP_MAIN);
            break;
        }
        case PRIVATE_EVENT_SCREEN_SETUP_LCD_CONTRAST:
            module_set_screen(SCREEN_SETUP_LCD_CONTRAST);
            break;
        case PUBLIC_EVENT_DONE:
        case PUBLIC_EVENT_RETURN:
            module_pop();
            break;
        default:
            module_process_event_common(&module_setup, event_id);
    }
}

static const struct screen *module_setup_search_screen(char screen_id)
{
    return module_search_screen(screen_id, MAX_SCREEN_MODULE_SETUP,
                                &module_setup,
                                screens_arr);
}

static void module_setup_set_data(char data_id, void *ptr)
{
    debug_print("module_setup_set_data id=%d ptr=%p\n",
                data_id,ptr);
    console_put_msg(STR_SCR_MODULE_SET_DATA, data_id);
}

static void *module_setup_get_data(char data_id)
{
    debug_print("module_main_get_data id=%d\n",
                data_id);
    console_put_msg(STR_SCR_MODULE_GET_DATA, data_id);
    return NULL;
}

PROGMEM const struct module module_setup=
{
    .init=module_setup_init,
    .process_event=module_setup_process_event,
    .search_screen=module_setup_search_screen,
    .get_data=module_setup_get_data,
    .set_data=module_setup_set_data,
    .name=STR_SCR_MODULE_SETUP_NAME,
};
