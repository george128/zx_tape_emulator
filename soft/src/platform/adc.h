#ifndef _PATFORM_ADC_H_
#define _PATFORM_ADC_H_

void adc_init();
void adc_stop();

unsigned char adc_get_value();

#endif /*_PATFORM_ADC_H_*/
