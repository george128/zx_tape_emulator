#include <keyboard/keyboard.h>
#include <platform/utils.h>
#include <utils/sleep.h>

#include "widget.h"
#include "line_edit.h"

#include <string.h>

static void g_line_edit_redraw(struct g_line_edit *line_edit)
{
    clear_region(line_edit->start_pos.x, line_edit->start_pos.y,
                 line_edit->start_pos.x+CHAR_WIDTH*(line_edit->max_pos), 
                 line_edit->start_pos.y+LINE_HEIGHT-1);
    draw_text(line_edit->start_pos.x, line_edit->start_pos.y, line_edit->buf, 0, 0);
    draw_line(line_edit->start_pos.x, line_edit->start_pos.y+LINE_HEIGHT-1,
              line_edit->start_pos.x+CHAR_WIDTH*(line_edit->max_pos),
              line_edit->start_pos.y+LINE_HEIGHT-1);
    flush_screen();
}

void g_line_edit_init(char *str_init, char max_len, char x_start, char y_start, 
                      struct g_line_edit *line_edit)
{
    if(max_len > SCREEN_WIDTH_CHARS)
        max_len = SCREEN_WIDTH_CHARS;

    line_edit->max_pos=max_len;
    
    if(str_init != NULL && *str_init!=0)
        strncpy(line_edit->buf, str_init, max_len);
    else
        memset(line_edit->buf,0,line_edit->max_pos);
    
    line_edit->cur_pos=strlen(line_edit->buf);
    line_edit->start_pos.x = x_start;
    line_edit->start_pos.y = y_start;
    line_edit->is_start = 1;
    
    g_line_edit_redraw(line_edit);
}

void g_line_edit_process_key(char keycode, struct g_line_edit *line_edit)
{
    debug_print("g_line_edit_process_key is_start=%d keycode=0x%02x cur_pos=%d\n",
                line_edit->is_start, keycode, line_edit->cur_pos);
    if(keycode == 0x0a) {
        ;/*skip return*/
    } else if(keycode == KEY_BS) {
        debug_puts("KEY_BS\n");
        if(line_edit->is_start) {
            line_edit->cur_pos=0;
            memset(line_edit->buf,0,line_edit->max_pos);
            g_line_edit_redraw(line_edit);
        } else if(line_edit->cur_pos>0) {
            line_edit->buf[--line_edit->cur_pos]=0;
            g_line_edit_redraw(line_edit);
        }
        debug_print("buf='%s'\n", line_edit->buf);
    } else if(line_edit->cur_pos<line_edit->max_pos-1) {
        line_edit->buf[line_edit->cur_pos++]=keycode;
        g_line_edit_redraw(line_edit);
    }

    line_edit->is_start = 0;
}

char *g_line_edit_get_str(struct g_line_edit *line_edit)
{
    return line_edit->buf;
}

void g_line_edit_done(struct g_line_edit *line_edit)
{
    clear_region(line_edit->start_pos.x, line_edit->start_pos.y+LINE_HEIGHT-1,
                 line_edit->start_pos.x+CHAR_WIDTH*(line_edit->max_pos), 
                 line_edit->start_pos.y+LINE_HEIGHT);
    flush_screen();
}
