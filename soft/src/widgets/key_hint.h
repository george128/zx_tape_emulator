#ifndef _KEY_HINT_H_
#define _KEY_HINT_H_

struct key_hint {
    unsigned char left_chars[4];
    unsigned char right_chars[4];
};

void key_hint_display(struct key_hint *data);
void key_hint_key_press(unsigned char is_right, unsigned char key_num);

#endif /*_KEY_HINT_H_*/
