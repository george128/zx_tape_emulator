#include <avr/io.h>
#include <stdlib.h>

#include <platform/utils.h>
#include <console/console.h>
#include <timer/timer.h>

#include "keyboard.h"

#define KEYB_BITS _BV(2)|_BV(3)|_BV(4)|_BV(5)|_BV(6)

PROGMEM const static char STR_INIT_KEYBOARD[]="Initializing keyboard...\n";

static void (*current_key_handler)(char flags) = NULL;

static struct timer timer_keyb;

static void avr_keyboard_timer_callback(unsigned char timer_id, const void *data)
{
    if(current_key_handler != NULL)
        current_key_handler(PINC|(~(KEYB_BITS)));
}


void avr_keyboard_init()
{
    console_puts_P(STR_INIT_KEYBOARD);
    DDRC&=~(KEYB_BITS);
    PORTC|=KEYB_BITS;
    timer_keyb.period=50;
    timer_keyb.callback=avr_keyboard_timer_callback;
    timer_setup(&timer_keyb);
}

void avr_set_key_handler(void (*key_handler)(char flags)) {
    current_key_handler = key_handler;
}
