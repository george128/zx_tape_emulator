#include "keyboard.h"

#include <platform/keyboard.h>
#include <platform/utils.h>
#include <console/console.h>

#undef DEBUG_KEYBOARD

#define BUF_LEN 4

PROGMEM static const char STR_KEYB_INIT[]="keyboard: init\n";

static char ring_buf[BUF_LEN];
static unsigned char buf_start, buf_end;
static unsigned char latest_flags;

static void store_key(char keycode)
{
    unsigned char buf_end_new=buf_end+1;
    if(buf_end_new==BUF_LEN)
        buf_end_new=0;

    if(buf_end_new==buf_start) {
        /* skip overflowed char */
        return;
    }
    
    ring_buf[buf_end]=keycode;
    buf_end=buf_end_new;
}

static void key_handler(char flags)
{
    int i;
    debug_print("flags=%02x latest_flags=%02x\n", (unsigned char)flags, latest_flags);
    if(flags == latest_flags)
        return;
    for(i=0;i<8;i++) {
        char mask=1<<i;
        if(flags & mask && !(latest_flags & mask)) {
#ifdef DEBUG_KEYBOARD
    	    console_puts("fl=");
    	    console_putd(flags);
    	    console_puts(" lfl=");
    	    console_putd(latest_flags);
    	    console_puts(" i=");
    	    console_putd((char)i);
    	    console_putc('\n');
#endif
            store_key(i);
        }
    }
    latest_flags = flags;
}


void keyb_init()
{
    console_puts_P(STR_KEYB_INIT);
    avr_keyboard_init();
    avr_set_key_handler(key_handler);
    buf_start=buf_end=0;
    latest_flags=0xff;
}


char kbhit()
{
    return buf_start!=buf_end;
}

char get_keycode()
{
    char c;
    unsigned char buf_start_new;
    while(!kbhit()); /*wait for a key*/
    c=ring_buf[buf_start];
    buf_start_new=buf_start+1;
    if(buf_start_new==BUF_LEN)
        buf_start_new=0;
    buf_start=buf_start_new;
    return c;
}
