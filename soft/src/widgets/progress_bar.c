#include <string.h>
#include <platform/utils.h>
#include <graphics/graphics.h>
#include <console/console.h>
#include "progress_bar.h"

#undef PB_DEBUG

#ifdef PB_DEBUG
PROGMEM static const char STR_PB[]  = "ProgressBar: ";
#endif

void set_progress_bar(struct progress_bar *state)
{
    short width=(state->is_progmem?strlen_P(state->text):strlen(state->text))*CHAR_WIDTH;
    short widthPercentage=width*state->percentage/100;
#ifdef PB_DEBUG
    console_puts_P(STR_PB);
    console_put_short(width);
    console_putc(' ');
    console_putd10(state->percentage);
    console_putc(' ');
    console_puts(state->text);
    console_putc(' ');
    console_putd10(state->is_progmem);
    console_putc(' ');
    console_putd10(state->x);
    console_putc(' ');
    console_putd10(state->y);
    console_putc(' ');
    console_putd10(widthPercentage);
    console_putc(' ');
    console_putd10(LINE_HEIGHT);
    console_next_line();
#endif
    clear_region(state->x, state->y, state->x+width, state->y+LINE_HEIGHT);
    draw_text(state->x, state->y, state->text, 0, state->is_progmem);
    invert_region(state->x, state->y, widthPercentage+state->x, state->y+LINE_HEIGHT);
}
