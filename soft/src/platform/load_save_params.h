#ifndef _LOAD_SAVE_PARAMS_H__
#define _LOAD_SAVE_PARAMS_H_

struct params_load {
    unsigned short pilot;
    unsigned short sync1;
    unsigned short sync2;
    unsigned short data1;
    unsigned short data0;
    char duration_header;
    char duration_data;
    unsigned short pause_block;
};

struct params_save {
    unsigned short pilot_max;
    unsigned short pilot_min;
    unsigned short sync1_max;
    unsigned short sync_total_max;
    unsigned short zero_threshold;
    unsigned short data_total_max;
    unsigned short min_pilot_count;
};

char get_params_load(struct params_load *p);
char get_params_save(struct params_save *p);

char save_params_load(struct params_load *p);
char save_params_save(struct params_save *p);

#endif /*_LOAD_SAVE_PARAMS_H_*/
