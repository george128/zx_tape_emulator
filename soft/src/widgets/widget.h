#ifndef _WIDGET_H_
#define _WIDGET_H_

struct pos {
    unsigned char x;
    unsigned char y;
};

struct size {
    unsigned char w;
    unsigned char h;
};

struct dim {
    struct pos pos;
    struct size size;
};

#endif /*_WIDGET_H_*/
