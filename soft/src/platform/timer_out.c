#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <string.h>
#include <stdlib.h>

#include <console/console.h>
#include <platform/settings.h>

#include "timer_out.h"

#define PIN_OUT _BV(3)
#define PIN_BUZ _BV(4)

#define BUF_STATE_FREE 0
#define BUF_STATE_READY 1
#define BUF_STATE_BUSY 2

#define STATE_PILOT 1
#define STATE_SYN1 5
#define STATE_SYN2 4
#define STATE_DATA_HIGH 3
#define STATE_DATA_LOW 2
#define STATE_WAIT 0
#define STATE_ERROR 7
/*
#define TICKS_PILOT 9911 //9834
#define TICKS_SYN1 3049 //3025
#define TICKS_SYN2 3360 //3334
#define TICKS_1 7817 //7756
#define TICKS_0 3909 //3879

#define PILOT_HEAD_LEN 1612*5 
#define PILOT_DATA_LEN 1612*2
*/
PROGMEM static const char STR_TIMER_OUT_INIT[]  = "timer_out_init()\n";
PROGMEM static const char STR_TIMER_OUT_DONE[]  = "timer_out_done()\n";
PROGMEM static const char STR_TIMER_OUT_PILOT[]  = "Pilot type: ";
PROGMEM static const char STR_TIMER_OUT_WAIT_DONE[]  = "Wait Done\n";
PROGMEM static const char STR_TIMER_OUT_WAIT2_DONE[]  = "BD\n";
PROGMEM static const char STR_TIMER_OUT_BUF[]  = "buf[";
PROGMEM static const char STR_TIMER_PILOT_UNKNOWN[]  = "Uknown pilot: ";
PROGMEM static const char STR_ERROR_STATE[]  = "ERROR STATE!!!\n";
PROGMEM static const char STR_ERROR_NO_STATE[]  = "STATE IS NULL!!!\n";

static struct timer_out_state *state;

static inline void set_state(char new_state)
{
    state->state=new_state;
    PORTK=new_state;
}

void timer_out_init(struct timer_out_state *state_init)
{
    struct settings s;

    get_params_load(&state_init->params_load);
    state=state_init;
    get_settings(&s);
    state->sound_on = s.sound_on;
    memset(state->outbuf,0,sizeof(*state->outbuf)*2);
    state->outbuf[0].flags=state->outbuf[1].flags=BUF_STATE_FREE;
    DDRK=0xff;
    set_state(STATE_WAIT);
    state->bit_pos=state->buf_no=0;
    DDRE|=_BV(3)|_BV(4);
    PORTE&=~(_BV(3)|_BV(4));
}

void timer_out_done()
{
    console_puts_P(STR_TIMER_OUT_DONE);
    TCCR3B=0; /*stop timer*/
    TIMSK3=0; /*disable timer interrupts*/
    state=NULL;
}

static char get_free_buf_no()  __attribute__ ((noinline));
static char get_free_buf_no() {
    if(state->outbuf[0].flags==BUF_STATE_FREE)
        return 0;
    if(state->outbuf[1].flags==BUF_STATE_FREE)
        return 1;
    return 0x7f;
}

static void timer_out_timer_init()
{
    console_puts_P(STR_TIMER_OUT_INIT);
    /*use timer3 mode4 (CTC) with OCR3A*/
    OCR3A=state->params_load.pilot;
    TCCR3A=0; /*no Compare Output mode */
    TCCR3B=_BV(WGM32)|_BV(CS30); /*MODE4 (CTC) and CLKio/1*/
    TCCR3C=0; /*no force output compare*/
    TIMSK3=_BV(OCIE3A);
    PORTE&=~_BV(3);
    state->pin_state=0;
    state->bit_pos=0;
    if(state->state != STATE_WAIT) {
        console_puts_P(STR_ERROR_STATE);
    }
    state->state=STATE_PILOT;
    sei(); /*enable global interrupts*/
}

char timer_out_process_block(char pilot_type, char *buf, unsigned short buf_len)
{
    if(state == NULL) {
        console_puts_P(STR_ERROR_NO_STATE);
        return TIMER_OUT_RES_ERR;
    }
    if(state->state==STATE_WAIT || pilot_type!=PILOT_TYPE_NONE)
        console_put_msg(STR_TIMER_OUT_PILOT, pilot_type);
    

    switch(pilot_type) {
        case PILOT_TYPE_NONE:
            if(buf_len == 0) {
                switch(state->state) {
                    case STATE_WAIT:
                        console_puts_P(STR_TIMER_OUT_WAIT_DONE);
                        return TIMER_OUT_RES_OK;
                    case STATE_ERROR:
                        console_puts_P(STR_ERROR_STATE);
                        return TIMER_OUT_RES_ERR;
                    default:
                        return TIMER_OUT_RES_BUSY;
                }
            }
            break;
        case PILOT_TYPE_HEAD:
            if(state->state!=STATE_WAIT) {
                return TIMER_OUT_RES_BUSY;
            }
            state->pilot_pos=state->params_load.duration_header*1612;
            timer_out_timer_init();
            break;
        case PILOT_TYPE_DATA:
            if(state->state!=STATE_WAIT) {
                return TIMER_OUT_RES_BUSY;
            }
            state->pilot_pos=state->params_load.duration_data*1612;
            timer_out_timer_init();
            break;
        default:
            state->state=STATE_ERROR;
            console_put_msg(STR_TIMER_PILOT_UNKNOWN, pilot_type);
            return TIMER_OUT_RES_ERR;
    }

    volatile char new_buf_no;
    
    if((new_buf_no=get_free_buf_no())==0x7f) {
        return TIMER_OUT_RES_BUSY;
    }
    console_puts_P(STR_TIMER_OUT_WAIT2_DONE);

    console_puts_P(STR_TIMER_OUT_BUF);
    console_putd(new_buf_no);
    console_putc(']');
    console_putc('=');
    console_put_short(buf_len);
    console_next_line();

    memcpy(state->outbuf[new_buf_no].buf, buf, buf_len);
    state->outbuf[new_buf_no].len=buf_len;
    state->outbuf[new_buf_no].pos=0;
    state->outbuf[new_buf_no].flags=BUF_STATE_READY;

    return TIMER_OUT_RES_OK;
}

/*
                 ZX 3.5MHZ               AVR 16MHZ

  pilot:        2168T                 | 9911T
  pilot-header: 2168T*8063            | 9911T*8063 start with low
  pilot-data:   2168T*3223            | 9911T*3223 start with low
  1-synchro:    high-667T             | high-3049T
  2-synchro:    low-735T              | low-3360T
  one:          high-low 1710T+1710T  | high-low 7817T*2
  zero:         high-low 855T+855T    | high-low 3909T*2 (3908,57)
*/

ISR(TIMER3_COMPA_vect) {
    if(state == NULL) {
        console_puts_P(STR_ERROR_NO_STATE);
        return;
    }
    if(state->pin_state) {
        PORTE&=~PIN_OUT;
        if(state->sound_on)
            PORTE&=~PIN_BUZ;
    } else {
        PORTE|=PIN_OUT;
        if(state->sound_on)
            PORTE|=PIN_BUZ;
    }
    switch(state->state) {
        case STATE_PILOT:
            state->pin_state^=0xff;
            if(state->outbuf[state->buf_no].flags == BUF_STATE_READY) {
                state->outbuf[state->buf_no].flags = BUF_STATE_BUSY;
            }

            if(state->pilot_pos--==0) {
                set_state(STATE_SYN1);
                state->pin_state=0xff;
                OCR3A=state->params_load.sync1;
            }
            break;
        case STATE_SYN1:
            set_state(STATE_SYN2);
            state->pin_state=0;
            OCR3A=state->params_load.sync2;;
            break;
        case STATE_SYN2:
        case STATE_DATA_LOW:
            set_state(STATE_DATA_HIGH);
            state->pin_state=0xff;
            {
                struct outbuf *ob=&state->outbuf[state->buf_no];
                char byte=ob->buf[ob->pos];
                char bit=byte&(1<<(7-state->bit_pos));
                if(bit)
                    OCR3A=state->params_load.data1;
                else
                    OCR3A=state->params_load.data0;
            }
            break;
        case STATE_DATA_HIGH:
            state->pin_state=0;
            if(state->bit_pos==7) {
                unsigned short *buf_pos=&state->outbuf[state->buf_no].pos;
                state->bit_pos=0;
                if(++(*buf_pos) == state->outbuf[state->buf_no].len) {
                    char new_buf_no=state->buf_no?0:1;
                    state->outbuf[state->buf_no].flags = BUF_STATE_FREE;
                    
                    switch(state->outbuf[new_buf_no].flags) {
                        case BUF_STATE_READY:
                            state->outbuf[new_buf_no].flags = BUF_STATE_BUSY;
                            state->buf_no = new_buf_no;
                            state->bit_pos=0;
                            break;
                        case BUF_STATE_FREE:
                            state->buf_no=0;
                            set_state(STATE_WAIT);
                            return;
                        case BUF_STATE_BUSY:
                            TCCR3B=0; /*stop timer*/
                            set_state(STATE_ERROR);
                            return;
                    }
                }
            } else 
                state->bit_pos++;
            set_state(STATE_DATA_LOW);
            break;
        case STATE_WAIT:
            TCCR3B=0; /*stop timer*/
            break;
    }
}
