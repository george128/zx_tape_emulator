#ifndef _PCF8583_H_
#define _PCF8583_H_

#include "rtc.h"

#define PCF8583_MODE_32K 0
#define PCF8583_MODE_50 1
#define PCF8583_MODE_EVENT_CNT 2
#define PCF8583_MODE_TEST 3

struct pcf8583_status {
    unsigned int timer_flag:1;
    unsigned int alarm_flag:1;
    unsigned int alarm_enabl:1;
    unsigned int mask:1;
    unsigned int func:2;
    unsigned int hold:1;
    unsigned int stop:1;
}__attribute__ ((packed));

struct pcf8583_hours {
    unsigned int h:4;
    unsigned int h10:2;
    unsigned int am_pm:1;
    unsigned int f12_24:1;
}__attribute__ ((packed));

struct pcf8583_year {
    unsigned int day:4;
    unsigned int day10:2;
    unsigned int year:2;
}__attribute__ ((packed));

struct pcf8583_month {
    unsigned int month:4;
    unsigned int month10:1;
    unsigned int weekday:3;
}__attribute__ ((packed));

struct pcf8583_bcd {
    unsigned int v:4;
    unsigned int v10:4;
}__attribute__ ((packed));

union pcf8583_regs {
    struct {
        struct pcf8583_status stat;
        struct pcf8583_bcd msec;
        struct pcf8583_bcd sec;
        struct pcf8583_bcd min;
        struct pcf8583_hours hour;
        struct pcf8583_year year3;
        struct pcf8583_month month;
        char unused[9];
        unsigned short year;
    }__attribute__ ((packed)) regs;
    char buf[18];
}__attribute__ ((packed));

void pcf8583_init();
void pcf8583_set_date(struct rtc *rtc);
void pcf8583_get_date(struct rtc *rtc);

void pcf8583_test();

#endif /*_PCF8583_H_*/
