#include <screens/shared_mem.h>
#include <platform/load_save_params.h>
#include <platform/utils.h>
#include <console/console.h>
#include "screen_setup_save.h"
#include "screen_setup_params_base.h"

struct data {
    struct params_data params_data;
    struct params_save params_save;
};

#define shm ((struct data *) (get_shared_mem()))

PROGMEM static const char STR_SCREEN_SAVE_PARAM_PILOT_MAX[]       = "PilotMx:";
PROGMEM static const char STR_SCREEN_SAVE_PARAM_PILOT_MIN[]       = "PilotMn:";
PROGMEM static const char STR_SCREEN_SAVE_PARAM_SYNC1_MAX[]       = "Sync1Mx:";
PROGMEM static const char STR_SCREEN_SAVE_PARAM_SYNC_TOATL_MAX[]  = "SncTtMx:";
PROGMEM static const char STR_SCREEN_SAVE_PARAM_ZERO_THRESHOLD[]  = "ZrThrsl:";
PROGMEM static const char STR_SCREEN_SAVE_PARAM_DATA_TOTAL_MAX[]  = "DtTotMx:";
PROGMEM static const char STR_SCREEN_SAVE_PARAM_MIN_PILOT_CNT[]   = "PltCtMn:";

PROGMEM static const struct param_rec param_recs_save[] = {
    {
        .text=STR_SCREEN_SAVE_PARAM_PILOT_MAX,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_save, pilot_max),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_SAVE_PARAM_PILOT_MIN,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_save, pilot_min),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_SAVE_PARAM_SYNC1_MAX,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_save, sync1_max),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_SAVE_PARAM_SYNC_TOATL_MAX,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_save, sync_total_max),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_SAVE_PARAM_ZERO_THRESHOLD,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_save, zero_threshold),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_SAVE_PARAM_DATA_TOTAL_MAX,
        .postfix=STR_POSTFIX_US,
        .offset=offsetof(struct params_save, data_total_max),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
        .conv_to=params_conv_to_us,
        .conv_from=params_conv_from_us,
    },
    {
        .text=STR_SCREEN_SAVE_PARAM_MIN_PILOT_CNT,
        .offset=offsetof(struct params_save, min_pilot_count),
        .to_str=params_short_to_str,
        .from_str=params_short_from_str,
    },
};

static void screen_setup_save_save_params(void *params)
{
    save_params_save(params);
}

static void screen_setup_save_get_params(void *params)
{
    get_params_save(params);
}

static void screen_setup_save_display()
{
    shm->params_data.save_params=screen_setup_save_save_params;
    shm->params_data.get_params=screen_setup_save_get_params;
    shm->params_data.params=&shm->params_save;
    screen_setup_params_init(&shm->params_data, param_recs_save, 7);
}

static void screen_setup_save_destroy()
{
}

static void screen_setup_save_key_press(char keycode)
{
    screen_setup_params_key_press(&shm->params_data, keycode);
}

PROGMEM const struct screen screen_setup_save=
{
    .display=screen_setup_save_display,
    .destroy=screen_setup_save_destroy,
    .key_press=screen_setup_save_key_press,
    .shm_size=sizeof(struct data)
};
