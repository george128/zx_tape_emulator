#include <string.h>
#include <assert.h>

#include <screens/screen.h>
#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <console/console.h>
#include <utils/strings.h>

#define TASK_IDLE 0
#define TASK_SET_SCREEN 1
#define TASK_PROCESS_EVENT 2
#define TASK_PROCESS_KEY_PRESS 4


#define MODULES_STACK_SIZE 4

static struct module modules[MODULES_STACK_SIZE];
static struct screen cur_scr;
static char task;
static char event_id;
static char screen_id;
static char keycode;
static unsigned char module_idx;

#ifdef MODULE_DEBUG

PROGMEM static const char STR_MODULE_TASK[]="module_task: task=";
PROGMEM static const char STR_MODULE_TASK_DONE[]="module_task: done task=";
PROGMEM static const char STR_MODULE_PROCESS_EVENT[]="module_task: process_event ";
PROGMEM static const char STR_MODULE_SET_SCREEN[]="module_task: set_screen ";
PROGMEM static const char STR_MODULE_PROCESS_KEY[]="module_task: process_key_press ";
PROGMEM static const char STR_MODULE_SCREEN_ADDR[]="module_task: screen addr=";
PROGMEM static const char STR_SCR_MODULE_MAIN_SCREEN_ID[]=": screen id=";
PROGMEM static const char STR_SCR_MODULE_MAIN_SCREEN_ADDR[]=" addr=";

#define module_debug console_put_msg

#else

#define STR_MODULE_TASK
#define STR_MODULE_TASK_DONE
#define STR_MODULE_PROCESS_EVENT
#define STR_MODULE_SET_SCREEN
#define STR_MODULE_PROCESS_KEY
#define STR_MODULE_SCREEN_ADDR
#define module_debug

#endif


PROGMEM static const char STR_SCR_MODULE_PUSH[]="module_push: ";
PROGMEM static const char STR_SCR_MODULE_PUSH_ERR[]="module_push: stack overflow!!!\n";
PROGMEM static const char STR_SCR_MODULE_POP[]="podule_pop: ";
PROGMEM static const char STR_SCR_MODULE_POP_ERR[]="podule_pop: stack underflow!!!\n";
PROGMEM static const char STR_SCR_MODULE_SET_DATA_ERROR[]="module_set_data error: idx=";
PROGMEM static const char STR_SCR_MODULE_GET_DATA_ERROR[]="module_get_data error: idx=";
PROGMEM static const char STR_SCR_MODULE_TASK_ERROR[]="module_task error: idx=";
PROGMEM static const char STR_SCR_MODULE_SCREEN_ERROR[]="module_task: can't set screen: ";
PROGMEM static const char STR_SCR_MODULE_COMMON_EVENT1[]="module_process_event_common: module=";
PROGMEM static const char STR_SCR_MODULE_COMMON_EVENT2[]=" unknown event: ";
PROGMEM static const char STR_SCR_MODULE_UNKNOWN_SCREEN1[]="module: ";
PROGMEM static const char STR_SCR_MODULE_UNKNOWN_SCREEN2[]=" search_screen: unknown screen_id ";
PROGMEM static const char STR_SCR_MODULE_SCREEN_SHM[]="screen shm: ";

void module_init()
{
    task=TASK_IDLE;
    module_idx=255;
    memset(modules,0,sizeof(modules));
    memset(&cur_scr,0,sizeof(cur_scr));
}

void module_set_screen(char _screen_id)
{
    screen_id=_screen_id;
    task|=TASK_SET_SCREEN;
}
void module_process_event(char _event_id)
{
    event_id=_event_id;
    task|=TASK_PROCESS_EVENT;
}

void module_key_press(char _keycode)
{
    keycode=_keycode;
    task|=TASK_PROCESS_KEY_PRESS;
}

void module_pop()
{
    if(module_idx==0) {
        console_puts_P(STR_SCR_MODULE_POP_ERR);
        return;
    }
    module_idx--;
    console_puts_P(STR_SCR_MODULE_POP);
    console_puts_P(modules[module_idx].name);
    console_next_line();
    module_process_event(PUBLIC_EVENT_RETURN);
}

void module_push(const struct module *module)
{
    if(module_idx==MODULES_STACK_SIZE-1) {
        console_puts_P(STR_SCR_MODULE_PUSH_ERR);
        return;
    }
    module_idx++;
    memcpy_P(modules+module_idx,module,sizeof(*module));
    console_puts_P(STR_SCR_MODULE_PUSH);
    console_puts_P(modules[module_idx].name);
    console_next_line();
    modules[module_idx].init();
    module_process_event(PUBLIC_EVENT_START);
}

void module_set_data(char data_id, void *ptr)
{
    if(module_idx==255) {
        console_put_msg(STR_SCR_MODULE_SET_DATA_ERROR, module_idx);
        return;
    }
    if(modules[module_idx].set_data!=0)
        modules[module_idx].set_data(data_id,ptr);
}
void *module_get_data(char data_id)
{
    if(module_idx==255) {
        console_put_msg(STR_SCR_MODULE_GET_DATA_ERROR, module_idx);
        return 0;
    }
    if(modules[module_idx].get_data!=0)
        return modules[module_idx].get_data(data_id);

    return 0;
}

void module_task()
{
    if(task==TASK_IDLE)
        return;
    if(module_idx==255) {
        console_put_msg(STR_SCR_MODULE_TASK_ERROR, module_idx);
        return;
    }
    module_debug(STR_MODULE_TASK, task);
    while(task&TASK_PROCESS_EVENT) {
        task&=~TASK_PROCESS_EVENT;
        module_debug(STR_MODULE_PROCESS_EVENT, event_id);
        modules[module_idx].process_event(event_id);
    }
    if(task&TASK_SET_SCREEN)
    {
        const struct screen *scr=modules[module_idx].search_screen(screen_id);
        console_puts_P(STR_MODULE_SCREEN_ADDR);
        console_putmem(scr, 4);
        console_next_line();
        module_debug(STR_MODULE_SET_SCREEN, screen_id);
        if(cur_scr.destroy!=0)
            cur_scr.destroy();
        if(scr!=0) {
            memcpy_P(&cur_scr,scr,sizeof(cur_scr));
            console_puts_P(STR_SCR_MODULE_SCREEN_SHM);
            console_put_short(cur_scr.shm_size);
            console_next_line();
            assert(cur_scr.shm_size < SHARED_MEM_SIZE);
            cur_scr.display();
        } else {
            memset(&cur_scr,0,sizeof(cur_scr));
            console_put_msg(STR_SCR_MODULE_SCREEN_ERROR, screen_id);
        }
        task&=~TASK_SET_SCREEN;
    }
    if(task&TASK_PROCESS_KEY_PRESS) {
        module_debug(STR_MODULE_PROCESS_KEY, keycode);
        if(cur_scr.key_press!=0)
            cur_scr.key_press(keycode);
        task&=~TASK_PROCESS_KEY_PRESS;
    }
    module_debug(STR_MODULE_TASK_DONE, task);
}

void module_process_event_common(const struct module *module, char event_id)
{
    const char *name=(const char *)pgm_read_word(&module->name);
    console_puts_P(STR_SCR_MODULE_COMMON_EVENT1);
    console_puts_P(name);
    console_put_msg(STR_SCR_MODULE_COMMON_EVENT2, event_id);
}

void module_unknown_screen(const struct module *module, char screen_id)
{
    const char *name=(const char *)pgm_read_word(&module->name);
    console_puts_P(STR_SCR_MODULE_UNKNOWN_SCREEN1);
    console_puts_P(name);
    console_put_msg(STR_SCR_MODULE_UNKNOWN_SCREEN2, screen_id);
}

const struct screen *module_search_screen(char screen_id, char max_screen,
                                          const struct module *module,
                                          PGM_P const *screens_arr)
{
    if(screen_id > max_screen || screen_id < 0) {
        module_unknown_screen(module, screen_id);
        return NULL;
    } else {
        struct screen *scr;
        scr=(struct screen *)pgm_read_word(&screens_arr[screen_id]);
        module_debug_print_screen(module, scr, screen_id);
        return scr;
    }

}

#ifdef MODULE_DEBUG
void module_debug_print_screen(const struct module *module, const struct screen *scr, char screen_id)
{
    const char *name=(const char *)pgm_read_word(&module->name);
    console_puts_P(name);
    console_puts_P(STR_SCR_MODULE_MAIN_SCREEN_ID);
    console_putd(screen_id);
    console_puts_P(STR_SCR_MODULE_MAIN_SCREEN_ADDR);
    console_putmem(&scr, 4);
    console_next_line();
}
#endif /*MODULE_DEBUG*/
