#include <platform/i2c.h>
#include <console/console.h>
#include <util/delay.h>
#include <platform/utils.h>
#include <string.h>
#include "rtc.h"
#include "pcf8583.h"

#define PCF8583_DEBUG

#ifdef PCF8583_DEBUG
PROGMEM static const char STR_REGS[]="pcf8583: regs: ";
#endif

static void pcf8583_set_date_internal(union pcf8583_regs *regs)
{
    char c=-128; /*0x80 signed*/
#ifdef PCF8583_DEBUG
    console_puts_P(STR_REGS);
    console_putmem(regs->buf, sizeof(*regs));
    console_next_line();
#endif
    i2c_write(I2C_DEV_NO_PCF8583, 0, 1, &c); /*stop counting*/
    i2c_write(I2C_DEV_NO_PCF8583, 0, sizeof(*regs), regs->buf);
}

static void pcf8583_get_date_internal(union pcf8583_regs *regs)
{
    i2c_read(I2C_DEV_NO_PCF8583, 0, sizeof(*regs), regs->buf);
}

void pcf8583_init() 
{
    union pcf8583_regs regs;
    pcf8583_get_date_internal(&regs);
    if(regs.regs.stat.stop) {
        regs.regs.stat.stop=0;
        pcf8583_set_date_internal(&regs);
    }
}

void pcf8583_get_date(struct rtc *rtc)
{
    union pcf8583_regs regs;
    short year;

    pcf8583_get_date_internal(&regs);
    year=regs.regs.year+regs.regs.year3.year-2000;
    if(year<0)
        year=0;
    rtc->year10=(year/10)%10;
    rtc->year=year%10;
    rtc->month10=regs.regs.month.month10;
    rtc->month=regs.regs.month.month;
    rtc->day10=regs.regs.year3.day10;
    rtc->day=regs.regs.year3.day;
    rtc->hour10=regs.regs.hour.h10;
    rtc->hour=regs.regs.hour.h;
    rtc->min10=regs.regs.min.v10;
    rtc->min=regs.regs.min.v;
    rtc->sec10=regs.regs.sec.v10;
    rtc->sec=regs.regs.sec.v;
}

void pcf8583_set_date(struct rtc *rtc)
{
    union pcf8583_regs regs;
    short year;
    
    memset(&regs,0,sizeof(regs));
    year=2000+rtc->year10*10+rtc->year;
    regs.regs.year=year&~3;
    regs.regs.year3.year=year%3;
    regs.regs.month.month10=rtc->month10;
    regs.regs.month.month=rtc->month;
    regs.regs.year3.day10=rtc->day10;
    regs.regs.year3.day=rtc->day;
    regs.regs.hour.h10=rtc->hour10;
    regs.regs.hour.h=rtc->hour;
    regs.regs.min.v10=rtc->min10;
    regs.regs.min.v=rtc->min;
    regs.regs.sec.v10=rtc->sec10;
    regs.regs.sec.v=rtc->sec;
    pcf8583_set_date_internal(&regs);
}

void pcf8583_test()
{
    union pcf8583_regs regs;
    pcf8583_get_date_internal(&regs);
    pcf8583_set_date_internal(&regs);
    console_putd(sizeof(regs));
}
