#ifndef _SPI_H_
#define _SPI_H_

void spi_init(unsigned char clk_pol, unsigned char clk_pha);
unsigned char spi_send_receive(unsigned char wb);

#endif