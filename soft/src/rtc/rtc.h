#ifndef _RTC_H_
#define _RTC_H_

struct rtc {
    unsigned char year10;
    unsigned char year;
    unsigned char month10;
    unsigned char month;
    unsigned char day10;
    unsigned char day;
    unsigned char hour10;
    unsigned char hour;
    unsigned char min10;
    unsigned char min;
    unsigned char sec10;
    unsigned char sec;
};

void rtc_init();
void rtc_test();
void rtc_get(struct rtc *rtc);
void rtc_set(struct rtc *rtc);

#endif /*_RTC_H_*/
