#ifndef _HIGHLIGHTER_H_
#define _HIGHLIGHTER_H_

#include "widget.h"

void highlighter_set(struct dim *dimention);
void highlighter_move_x(struct dim *dim_orig, unsigned char new_x);
void highlighter_move_y(struct dim *dim_orig, unsigned char new_y);
#endif /*_HIGHLIGHTER_H_*/
