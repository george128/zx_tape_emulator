#include <string.h>
#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <graphics/graphics.h>
#include <widgets/progress_bar.h>
#include <keyboard/keyboard.h>
#include <console/console.h>
#include <zx_files/tap.h>
#include <disk/ff.h>
#include <disk/diskio.h>
#include <utils/strconv.h>
#include <utils/sleep.h>
#include <platform/systime.h>
#include <platform/timer_out.h>
#include <timer/timer.h>
#include "module_main.h"
#include "screen_tap.h"

#define MAX_BLOCKS 10

#define STATE_CHUNK_READ 0
#define STATE_CHUNK_SEND 1
#define STATE_WAIT_BLOCK 2
#define STATE_PAUSE 3
#define STATE_PAUSE_WAIT 4
#define STATE_PAUSE_KEYB 5
#define STATE_DONE 6
#define STATE_ERROR 7

struct data {
    char path[PATH_MAX];
    FATFS fatfs;
    FIL fil;
    struct tap_state tap_state;
    struct timer_out_state timer_out_state;
    long len;
    unsigned char part_type;
    struct timer timer_tap;
    struct timer timer_pause;
    unsigned char timer_id;
    unsigned char state;
    volatile char tap_ret;
    char num_blocks;
    char block_no;
    char block_show_start;
    char do_update;
    struct {
        char text[SCREEN_WIDTH_CHARS+1];
        unsigned short len;
        unsigned short pos;
        unsigned long file_pos;
    } blocks[MAX_BLOCKS];
};

#define shm ((struct data *) (get_shared_mem()))

PROGMEM static const char STR_SCREEN_TAP_FILE[]  = "File: ";
PROGMEM static const char STR_SCREEN_TAP_FILE_ERR[]  = "File err: ";
PROGMEM static const char STR_SCREEN_TAP_FILE_READ_ERR[]  = "File read err: ";
PROGMEM static const char STR_SCREEN_TAP_FILE_REWIND_ERR[]  = "File rewind err: ";
PROGMEM static const char STR_SCREEN_TAP_FILE_CLOSE[]  = "File close: ";
PROGMEM static const char STR_SCREEN_TAP_FILE_RES[]  = "File res: ";
PROGMEM static const char STR_SCREEN_TAP_UMOUNT[]  = "Umount: ";
PROGMEM static const char STR_SCREEN_TAP_TOTAL_LEN[]="Len: ";
PROGMEM static const char STR_SCREEN_TAP_FREAD_TIME[]  = "Read time(ms): ";
PROGMEM static const char STR_SCREEN_TAP_PROCESSING[]  = "Processing...";
PROGMEM static const char STR_SCREEN_TAP_HEADER_TYPE[]  = "Header type=";
PROGMEM static const char STR_SCREEN_TAP_BLOCK_LEN[]  = "BlockLen:";
PROGMEM static const char STR_SCREEN_TAP_ERROR_TAP[]  = "Error processing tap!!!\n";
PROGMEM static const char STR_SCREEN_TAP_RETURN[]  = "Left to return";
PROGMEM static const char STR_SCREEN_TAP_TAP_RET[]  = "wait_block: tap_ret=";
PROGMEM static const char STR_SCREEN_TAP_PAUSED[]  = "Paused\n";
PROGMEM static const char STR_SCREEN_TAP_UNPAUSED[]  = "UnPaused\n";
#ifdef SCR_TAP_DEBUG
PROGMEM static const char STR_SCREEN_TAP_BLOCKS[]  = "Blocks: ";
PROGMEM static const char STR_SCREEN_TAP_OFFSET[]  = " offset: ";
PROGMEM static const char STR_SCREEN_TAP_POS[]  = "pos=";
PROGMEM static const char STR_SCREEN_TAP_LEN[]  = " len=";
PROGMEM static const char STR_SCREEN_TAP_RB[]  = " rb=";
#endif
PROGMEM static const char STR_SCREEN_TAP_SEEK[]  = "File seek: ";

static short get_next_data(char *buf, short len)
{
    UINT rb;
    // unsigned short t_start=get_systime(), t_end;
    FRESULT res = f_read(&shm->fil,buf,len,&rb);
    // t_end=get_systime();
    if(res != FR_OK) {
        console_put_msg(STR_SCREEN_TAP_FILE_READ_ERR, res);
        return -1;
    }
#ifdef SCR_TAP_DEBUG
    console_puts_P(STR_SCREEN_TAP_FREAD_TIME);
    console_put_short(t_end-t_start);
    console_puts_P(STR_SCREEN_TAP_LEN);
    console_put_short(len);
    console_puts_P(STR_SCREEN_TAP_RB);
    console_put_short(rb);
    console_next_line();
#endif
    return rb;
}

static void screen_tap_progress_bar(char percentage, char base)
{
    char offset=shm->block_show_start;
    struct progress_bar pb= {
        .percentage=percentage,
        .text=shm->blocks[base].text,
        .is_progmem=0,
        .x=0,
        .y=line_y(base-offset),
    };
    clear_line(base-offset);
    set_progress_bar(&pb);

}

static void display_blocks()
{
    char num_blocks=shm->num_blocks==0?shm->block_no:shm->num_blocks;
    char i;
    char offset=shm->block_show_start;

#ifdef SCR_TAP_DEBUG
    console_puts_P(STR_SCREEN_TAP_BLOCKS);
    console_putd10(num_blocks);
    console_puts_P(STR_SCREEN_TAP_OFFSET);
    console_putd10(offset);
    console_next_line();
#endif
    for(i=offset;i<num_blocks;i++) {
        long percentage;
        if(i==shm->block_no && (shm->state==STATE_DONE || shm->state == STATE_PAUSE_KEYB)) {
            percentage=10;
        } else {
            percentage=(long)shm->blocks[i].pos*100/(long)shm->blocks[i].len;
        }
#ifdef SCR_TAP_DEBUG
        console_puts_P(STR_SCREEN_TAP_POS);
        console_put_short(shm->blocks[i].pos);
        console_puts_P(STR_SCREEN_TAP_LEN);
        console_put_short(shm->blocks[i].len);
        console_putc(' ');
        console_put_long(percentage);
        console_next_line();
#endif
        if(i-offset>=SCREEN_MAX_LINES-1)
            break;
        screen_tap_progress_bar(percentage, i);
    }
    flush_screen();
}

static void found_header1(struct tap_header *h, unsigned long file_pos)
{
    char buf[SCREEN_WIDTH_CHARS], *ptr=buf;
    char i=0,j=0;

    if(shm->block_no==MAX_BLOCKS)
        return;

    switch(h->type) {
        case 0: /*Programm*/
            *ptr='P';
            break;
        case 1: /*Numbers array*/
            *ptr='N';
            break;
        case 2: /*Charracters array*/
            *ptr='A';
            break;
        case 3: /*Code*/
            *ptr='C';
            break;
        default: /*Undefined*/
            *ptr='U';
    }
    ptr++;
    *(ptr++)=':';
    while((h->name[i]<0x20 || h->name[i]>0x7f) && i<10)i++;
    while(i<10 && j<6) {
        if(h->name[i]>=0x20 && h->name[i]<=0x7f) {
            *(ptr++)=h->name[i];
            j++;
        }
        i++;
    }
    
    *(ptr++)=':';
    {
        char buf_short[6];
        strncpy(ptr,short10_2_str(h->data_len,buf_short), 6);
    }
    strncpy(shm->blocks[shm->block_no].text, buf, SCREEN_WIDTH_CHARS);
    shm->blocks[shm->block_no].len=sizeof(*h);
    shm->blocks[shm->block_no].pos=0;
    shm->blocks[shm->block_no].file_pos=file_pos;
    shm->len+=shm->blocks[shm->block_no].len;
    shm->block_no++;
    display_blocks();
}

static void found_block1(unsigned short len, unsigned long file_pos)
{
    char buf[16], buf2[6];

    if(shm->block_no==MAX_BLOCKS)
        return;
    strncpy_P(buf,STR_SCREEN_TAP_BLOCK_LEN,12);
    strcat(buf,short10_2_str(len,buf2));
    strncpy(shm->blocks[shm->block_no].text, buf, SCREEN_WIDTH_CHARS);
    shm->blocks[shm->block_no].len=len;
    shm->blocks[shm->block_no].pos=0;
    shm->blocks[shm->block_no].file_pos=file_pos;
    shm->len+=shm->blocks[shm->block_no].len;
    shm->block_no++;
    display_blocks();
}

PROGMEM const struct tap_handler tap_handler1={
    .get_next_data=get_next_data,
    .found_header=found_header1,
    .found_block=found_block1,
};

static void found_header2(struct tap_header *h, unsigned long file_pos)
{
    shm->part_type=h->type;
    console_put_msg(STR_SCREEN_TAP_HEADER_TYPE,h->type);
    shm->block_no++;
    if(shm->block_no>=SCREEN_MAX_LINES)
        shm->block_show_start=shm->block_no-SCREEN_MAX_LINES+1;
    display_blocks();
}

static void found_block2(unsigned short len, unsigned long file_pos)
{
    shm->part_type=0xff;
    console_puts_P(STR_SCREEN_TAP_BLOCK_LEN);
    console_put_short(len);
    console_next_line();
    shm->block_no++;
    if(shm->block_no>=SCREEN_MAX_LINES)
        shm->block_show_start=shm->block_no-SCREEN_MAX_LINES+1;
    display_blocks();
}

PROGMEM const struct tap_handler tap_handler2={
    .get_next_data=get_next_data,
    .found_header=found_header2,
    .found_block=found_block2,
};

static char screen_tap_openfile() {
    char code=0;

    memset(&shm->fatfs, 0, sizeof(shm->fatfs));
    memset(&shm->fil, 0, sizeof(shm->fil));

    shm->fatfs.drv=0; /*drive 0*/
    FRESULT res=f_mount(&shm->fatfs,"0:",1);
    if(res!=FR_OK) {
        code = 10;
        console_put_msg(STR_SCREEN_TAP_FILE_RES, res);
        goto err;
    }
    res=f_open(&shm->fil,shm->path, FA_READ);
    if(res!=FR_OK) {
        code = 20;
        console_put_msg(STR_SCREEN_TAP_FILE_RES, res);
    }
 err:
    if(code!=0) {
        char buf[4];
        console_put_msg(STR_SCREEN_TAP_FILE_ERR, code);
        draw_text(size_x(0),line_y(3),STR_SCREEN_TAP_FILE_ERR,0,1);
        byte2dec(code, buf);
        draw_text(size_x(11),line_y(3),buf,0,0);
        flush_screen();
    }
    return code;

}

static void screen_tap_process_tap_err()
{
    console_puts_P(STR_SCREEN_TAP_ERROR_TAP);
    draw_text(size_x(0),line_y(SCREEN_MAX_LINES-1),STR_SCREEN_TAP_ERROR_TAP,1,1);
    flush_screen();
    timer_free(shm->timer_id);
    shm->timer_id=0;
}

static void screen_tap_callback_pause(unsigned char timer_id, const void *data)
{
    if(shm->state == STATE_PAUSE_WAIT) {
        shm->state=STATE_CHUNK_READ;
        timer_free(timer_id);
    }
}

static char screen_tap_get_pilot_type()
{
    if(!shm->tap_state.is_first)
        return PILOT_TYPE_NONE;
    return (shm->part_type==0)?PILOT_TYPE_HEAD:PILOT_TYPE_DATA;
}

static void screen_tap_display_pos()
{
    char buf[14], *ptr=buf, buf_short[6];
    *(ptr++)='[';
    ptr=byte2dec(shm->block_no, ptr);
    *(ptr++)=']';
    *(ptr++)=' ';
    strcpy(ptr,short10_2_str(shm->blocks[shm->block_no-1].pos,buf_short));
    clear_line(SCREEN_MAX_LINES-1);
    draw_text(size_x(0),
              line_y(SCREEN_MAX_LINES-1),
              buf,0,0);
    flush_screen();
}

static void screen_tap_callback(unsigned char timer_id, const void *data)
{
    char timer_ret;
    
 rep:
    switch(shm->state) {
        case STATE_CHUNK_SEND:
            timer_ret=timer_out_process_block(screen_tap_get_pilot_type(),
                                               shm->tap_state.buf,
                                               shm->tap_state.buf_len);
            switch(timer_ret) {
                case TIMER_OUT_RES_ERR:
                    shm->state=STATE_ERROR;
                    break;
                case TIMER_OUT_RES_OK:
                    shm->blocks[shm->block_no-1].pos=shm->tap_state.block_pos;
                    shm->do_update=1;
                    if(shm->tap_state.block_pos==shm->tap_state.block_len) {
                        shm->tap_state.block_len=0;
                        shm->state=STATE_WAIT_BLOCK;
                    } else {
                        shm->state=STATE_CHUNK_READ;
                        goto rep;
                    }
                    break;
                case TIMER_OUT_RES_BUSY:
                    if(shm->do_update) {
                        display_blocks();
                        screen_tap_display_pos();
                        shm->do_update=0;
                    }
                    //just do another try on the next call
                    break;
            }
            break;
        case STATE_CHUNK_READ:
        {
            struct tap_handler h;
            memcpy_P(&h,&tap_handler2, sizeof(h));
            shm->tap_ret=tap_process(&h, &shm->tap_state);
            switch(shm->tap_ret) {
                case TAP_STATE_PROCESS:
                    shm->state=STATE_CHUNK_SEND;
                    goto rep;
                    break;
                case TAP_STATE_DONE:
                    shm->state=STATE_WAIT_BLOCK;
                    break;
                case TAP_STATE_ERR:
                    shm->state=STATE_ERROR;
                    break;
            }
            break;
        }
        case STATE_WAIT_BLOCK:
            timer_ret=timer_out_process_block(PILOT_TYPE_NONE, NULL, 0);
            switch(timer_ret) {
                case TIMER_OUT_RES_OK:
                    console_put_msg(STR_SCREEN_TAP_TAP_RET, shm->tap_ret);
                    if(shm->tap_ret == TAP_STATE_DONE)
                        shm->state=STATE_DONE;
                    else
                        shm->state=STATE_PAUSE;
                    display_blocks();
                    screen_tap_display_pos();
                    break;
                case TIMER_OUT_RES_ERR:
                    shm->state=STATE_ERROR;
                    break;
                case TIMER_OUT_RES_BUSY:
                    // just wait
                    break;
            }
            break;
        case STATE_PAUSE:
            shm->state=STATE_PAUSE_WAIT;
            timer_setup(&shm->timer_pause);
            break;
        case STATE_PAUSE_WAIT:
            // just wait for another timer to change the state
            break;
        case STATE_PAUSE_KEYB:
            // just wait for beyboard command to release
            break;
        case STATE_ERROR:
            screen_tap_process_tap_err();
            break;
        case STATE_DONE:
            draw_text(size_x(0),line_y(SCREEN_MAX_LINES-1),STR_SCREEN_TAP_RETURN,0,1);
            flush_screen();
            timer_free(shm->timer_id);
            shm->timer_id=0;
            shm->block_no=0;
            break;
    }
}

static char screen_tap_find_blocks()
{
    char ret;
    struct tap_handler h;
    unsigned short t_start, t_end;
    char i, j, *ptr;

    memcpy_P(&h,&tap_handler1, sizeof(h));
    shm->tap_state.block_len=0;
    shm->tap_state.file_pos=0;
    t_start=get_systime();
    while((ret=tap_process(&h, &shm->tap_state))==TAP_STATE_PROCESS) {
        if(shm->tap_state.block_pos==shm->tap_state.block_len) {
            // shm->len+=shm->tap_state.block_len;
            shm->tap_state.block_len=0;
        }
    }
    if(ret==TAP_STATE_ERR) {
        console_puts_P(STR_SCREEN_TAP_ERROR_TAP);
        draw_text(size_x(0),line_y(SCREEN_MAX_LINES-1),STR_SCREEN_TAP_ERROR_TAP,1,1);
        flush_screen();
        return 0;
    }
    t_end=get_systime();
    console_puts_P(STR_SCREEN_TAP_FREAD_TIME);
    console_put_short(t_end-t_start);
    console_next_line();
    for(i=0;i<shm->block_no;i++) {
        ptr=shm->blocks[i].text;
        for(j=0;j<SCREEN_WIDTH_CHARS;j++) {
            if(*ptr==0)*ptr=' ';
            ptr++;
        }
        *ptr=0;
    }
    return 1;
}

static void screen_tap_display() 
{
    char *path=module_get_data(PRIVATE_DATA_EVENT_TAP_FILE);
    char buf[20], buf_long[20], *ptr=buf;

    memmove(shm->path, path, PATH_MAX);
    console_puts_P(STR_SCREEN_TAP_FILE);
    console_puts(shm->path);
    console_next_line();
    shm->block_no=0;
    shm->num_blocks=0;
    shm->len=0;
    shm->block_show_start=0;
    timer_out_init(&shm->timer_out_state);
    shm->timer_id=0;
    shm->state=STATE_DONE;

    clear_screen();
    draw_text(size_x(0),line_y(0),STR_SCREEN_TAP_PROCESSING,0,1);
    flush_screen();
    if(screen_tap_openfile()!=0)return;
    if(!screen_tap_find_blocks())return;
    shm->num_blocks=shm->block_no;
    shm->block_no=0;
    screen_tap_progress_bar(10, shm->block_no);
    draw_text(size_x(0),line_y(SCREEN_MAX_LINES-1),STR_SCREEN_TAP_TOTAL_LEN,0,1);
    *(ptr++)='[';
    ptr=byte2dec(shm->num_blocks,ptr);
    *(ptr++)=']';
    *ptr=0;
    ptr=long10_2_str(shm->len,buf_long);
    strncat(buf,ptr,19);
    draw_text(size_x(4),line_y(SCREEN_MAX_LINES-1),buf,0,0);
    flush_screen();

    shm->tap_state.block_len=0;
    shm->tap_state.buf_len=0;
    shm->timer_tap.callback=screen_tap_callback;
    shm->timer_tap.period=50;

    shm->timer_pause.callback=screen_tap_callback_pause;
    shm->timer_pause.period=2000;
}

static void screen_tap_destroy()
{
    FRESULT res=f_close(&shm->fil);
    console_put_msg(STR_SCREEN_TAP_FILE_CLOSE, res);
    res=f_mount(NULL,"0:",1); // umount
    console_put_msg(STR_SCREEN_TAP_UMOUNT, res);
    {
        char c=0;
        disk_ioctl(0,CTRL_POWER, &c);
    } 
    timer_out_done();
    if(shm->timer_id!=0) {
        timer_free(shm->timer_id);
    }
}

static char screen_tap_fseek(unsigned long pos)
{
    FRESULT res;
    console_puts_P(STR_SCREEN_TAP_SEEK);
    console_put_long(pos);
    console_next_line();
    if((res=f_lseek(&shm->fil,pos)) != FR_OK) {
        console_put_msg(STR_SCREEN_TAP_FILE_REWIND_ERR, res);
        clear_screen();
        draw_text(size_x(0),line_y(3),STR_SCREEN_TAP_FILE_REWIND_ERR,0,1);
        flush_screen();
        return 1;
    }
    shm->tap_state.file_pos=pos;
    return 0;
}

static void screen_tap_key_press(char keycode)
{
    switch(keycode) {
        case KEY_ENTER:
            switch(shm->state) {
                case STATE_DONE:
                    if(screen_tap_fseek(shm->blocks[shm->block_no].file_pos)!=0)return;
                    shm->state = STATE_CHUNK_READ;
                    shm->timer_id=timer_setup(&shm->timer_tap);
                    break;
                case STATE_PAUSE_WAIT:
                    shm->state = STATE_PAUSE_KEYB;
                    console_puts_P(STR_SCREEN_TAP_PAUSED);
                    clear_line(SCREEN_MAX_LINES-1);
                    draw_text(size_x(0),line_y(SCREEN_MAX_LINES-1),STR_SCREEN_TAP_PAUSED,0,1);
                    flush_screen();
                    break;
                case STATE_PAUSE_KEYB:
                    shm->state = STATE_PAUSE_WAIT;
                    console_puts_P(STR_SCREEN_TAP_UNPAUSED);
                    clear_line(SCREEN_MAX_LINES-1);
                    draw_text(size_x(0),line_y(SCREEN_MAX_LINES-1),STR_SCREEN_TAP_UNPAUSED,0,1);
                    flush_screen();
                    break;
                default:
                    console_putd(shm->state);
                    console_next_line();
                    shm->state = STATE_DONE;
            }
            break;
        case KEY_LEFT: 
        {
            char *p=strrchr(shm->path,'/');
            *p=0;
            module_set_data(PRIVATE_DATA_EVENT_TAP_FILE,shm->path);
            module_process_event(PUBLIC_EVENT_RETURN);
            break;
        }
        case KEY_UP:
            if(shm->state == STATE_DONE || shm->state == STATE_PAUSE_KEYB) {
                if(shm->block_no > 0) {
                    screen_tap_progress_bar(0, shm->block_no);
                    shm->block_no--;
                    if(shm->block_no-shm->block_show_start<0)
                        shm->block_show_start--;
                    // screen_tap_progress_bar(10, shm->block_no);
                    // flush_screen();
                    display_blocks();
                }
            }
            break;
        case KEY_DOWN:
            if(shm->state == STATE_DONE || shm->state == STATE_PAUSE_KEYB) {
                if(shm->block_no < shm->num_blocks-1) {
                    screen_tap_progress_bar(0, shm->block_no);
                    shm->block_no++;
                    if(shm->block_no-shm->block_show_start>=SCREEN_MAX_LINES-1)
                        shm->block_show_start++;
                    // screen_tap_progress_bar(10, shm->block_no);
                    // flush_screen();
                    display_blocks();
                }
            }
            break;
    }
}

PROGMEM const struct screen screen_tap=
{
    .display=screen_tap_display,
    .destroy=screen_tap_destroy,
    .key_press=screen_tap_key_press,
    .shm_size=sizeof(struct data)
};
