#ifndef _MODULE_MAIN_H_
#define _MODULE_MAIN_H_

#include <screens/scr_module.h>

#define PATH_MAX 255

#define SCREEN_WELCOME 0
#define SCREEN_FILES 1
#define SCREEN_TAP 2
#define SCREEN_SAVE 3
#define SCREEN_FILE_OPS 4

#define MAX_SCREEN_MODULE_MAIN 5

#define PRIVATE_EVENT_TAP 10
#define PRIVATE_EVENT_SCREEN_WELCOME_DONE 11
#define PRIVATE_EVENT_LOAD_FILE 12
#define PRIVATE_EVENT_SAVE_FILE 13
#define PRIVATE_EVENT_FILE_OPS 14
#define PRIVATE_EVENT_SETUP 15

#define PRIVATE_DATA_EVENT_TAP_FILE 10

extern const struct module module_main;

#endif /*_MODULE_MAIN_H_*/
