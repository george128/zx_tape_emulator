#ifndef _FONT5X8_H_
#define _FONT5X8_H_

#include <platform/utils.h>

#define FONT_WIDTH 5
#define FONT_HEIGH 8

/* Tablica czcionek 5x7  */
 
extern const unsigned char font5x8[] PROGMEM;

#endif /*_FONT5X8_H_*/

