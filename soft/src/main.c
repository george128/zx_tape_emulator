#include <graphics/graphics.h>
#include <keyboard/keyboard.h>
#include <timer/timer.h>
#include <screens/main/module_main.h>
#include <disk/ff.h>
#include <console/console.h>
#include <platform/utils.h>
#include <platform/systime.h>
#include <platform/i2c.h>

#include <stdlib.h>

PROGMEM static const char STR_MAIN_CONSOLE[]="console: '";
PROGMEM static const char STR_STARTING[]="Starting...\n";
PROGMEM static const char STR_MAIN_LOOP[]="Start main loop\n";

/*void main(void) __attribute__ ((naked));*/
void disk_init();

int main(void)
{
    console_init();
    systime_init();
    graphics_init();
    keyb_init();
    i2c_init();
    disk_init();

    // disk_test();
    // exit(0);

    module_init();
    console_puts_P(STR_STARTING);

    clear_screen();
    module_push(&module_main);

    console_puts_P(STR_MAIN_LOOP);
    for(;;) {
        char ch;
        if(kbhit()) {
            module_key_press(get_keycode());
        }
        if((ch=console_read(1))>0) {
            console_puts_P(STR_MAIN_CONSOLE);
            console_putc(ch);
            console_putc('\'');
            console_putc(' ');
            console_putd(ch);
            console_putc('\n');
            if(ch >= '1' && ch <= '8')
                module_key_press(ch - '1');
            else
                module_key_press(ch);
        }
        timer_task();
        module_task();
    } 

    return 0;
}
